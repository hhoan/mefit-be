﻿using AutoMapper;
using Mefit_BE.Models.Entities;
using Mefit_BE.Models.DTOs.MuscleGroups;

namespace Mefit_BE.Profiles
{
    public class MuscleGroupProfile : AutoMapper.Profile
    {
        public MuscleGroupProfile() 
        {
            CreateMap<MuscleGroupPostDTO, MuscleGroup>(); // <source,destination>

            CreateMap<MuscleGroup, MuscleGroupDTO>()
                .ForMember(dto => dto.Exercises, opt => opt
                .MapFrom(m => m.Exercises.Select(m => m.Exercise_id).ToList()));

            CreateMap<MuscleGroupPutDTO, MuscleGroup>();
        }
    }  
}
