﻿
using AutoMapper;
using Mefit_BE.Models.DTOs.WorkoutCategories;
using Mefit_BE.Models.Entities;

namespace Mefit_BE.Profiles
{
    public class WorkoutCategoryProfile : AutoMapper.Profile
    {
        public WorkoutCategoryProfile() 
        {
            CreateMap<WorkoutCategory, WorkoutCategoryDTO>();
            CreateMap<WorkoutCategoryPostDTO, WorkoutCategory>();
            CreateMap<WorkoutCategoryPutDTO, WorkoutCategory>();

        }
    }
}
