﻿using Mefit_BE.Models.DTOs.ProgramCategories;
using Mefit_BE.Models.Entities;

namespace Mefit_BE.Profiles
{
    public class ProgramCategoryProfile : AutoMapper.Profile
    {
        public ProgramCategoryProfile()
        {
            CreateMap<ProgramCategory, ProgramCategoryDTO>();
            CreateMap<ProgramCategoryPostDTO, ProgramCategory>();
            CreateMap<ProgramCategoryPutDTO, ProgramCategory>();
        }
    }
}
