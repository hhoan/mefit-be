﻿using AutoMapper;
using Mefit_BE.Models.DTOs.GoalDTOs;
using Mefit_BE.Models.DTOs.MuscleGroups;
using Mefit_BE.Models.Entities;

namespace Mefit_BE.Profiles
{
    public class GoalProfile : Profile
    {
        public GoalProfile() 
        {
            CreateMap<GoalPostDTO, Goal>(); // <source,destination>
            CreateMap<Goal, GoalDTO>()
                .ForMember(dto => dto.Program, opt => opt
                .MapFrom(p => p.Program));

            CreateMap<Goal, GoalSummaryDTO>();
            CreateMap<GoalPutDTO,Goal>();
        }
    }
}
