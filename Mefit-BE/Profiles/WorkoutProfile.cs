﻿
using Mefit_BE.Models.DTOs.Workouts;
using Mefit_BE.Models.Entities;

namespace Mefit_BE.Profiles
{
    public class WorkoutProfile : AutoMapper.Profile
    {
        public WorkoutProfile()
        {
            CreateMap<Workout, WorkoutDTO>()
                .ForMember(dto => dto.WorkoutCategory, opt => opt
                .MapFrom(w => w.WorkoutCategory))
                .ForMember(dto => dto.Sets, opt => opt
                .MapFrom(w => w.Sets.ToList()));

            CreateMap<WorkoutPostDTO, Workout>();
            CreateMap<WorkoutPutDTO, Workout>();
            CreateMap<Workout, WorkoutSummaryDTO>();
        }
    }
}
