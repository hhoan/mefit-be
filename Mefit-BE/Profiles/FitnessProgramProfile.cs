﻿
using Mefit_BE.Models.DTOs.FitnessPrograms;
using Mefit_BE.Models.Entities;

namespace Mefit_BE.Profiles
{
    public class FitnessProgramProfile : AutoMapper.Profile
    {
        public FitnessProgramProfile()
        {
            CreateMap<FitnessProgram, FitnessProgramDTO>()
                .ForMember(dto => dto.programCategory, opt => opt
                .MapFrom(p => p.programCategory))
                .ForMember(dto => dto.Workouts, opt => opt
                .MapFrom(p => p.Workouts.ToList()));

            CreateMap<FitnessProgram, FitnessProgramSummaryDTO>()
                .ForMember(dto => dto.Workouts, opt => opt
                .MapFrom(p => p.Workouts.ToList()));

            CreateMap<FitnessProgramPostDTO, FitnessProgram>(); 

            CreateMap<FitnessProgramPutDTO, FitnessProgram>();
        }
    }
}
