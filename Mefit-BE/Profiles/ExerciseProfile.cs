﻿using AutoMapper;
using Mefit_BE.Models.DTOs.Exercises;
using Mefit_BE.Models.Entities;
using Microsoft.JSInterop.Infrastructure;

namespace Mefit_BE.Profiles
{
    public class ExerciseProfile : AutoMapper.Profile
    {
        public ExerciseProfile() 
        {
            CreateMap<ExercisePostDTO, Exercise>(); // <source,destination>

            CreateMap<Exercise, ExerciseDTO>() // include musclegroups here and not only musclegroup ID?
                .ForMember(dto => dto.MuscleGroup_Id, opt => opt
                .MapFrom(e => e.MuscleGroup_Id))
                .ForMember(dto => dto.Sets, opt => opt
                .MapFrom(e => e.Sets.ToList()));

            CreateMap<ExercisePutDTO, Exercise>();
            CreateMap<Exercise, ExerciseSummaryDTO>();
        }
    }
}
