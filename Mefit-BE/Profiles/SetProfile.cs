﻿using Mefit_BE.Models.DTOs.Sets;
using Mefit_BE.Models.Entities;
using AutoMapper;


namespace Mefit_BE.Profiles
{
    public class SetProfile : AutoMapper.Profile
    {
        public SetProfile()
        {
            CreateMap<SetPostDTO, Set>(); // <source,destination>
            
            CreateMap<Set, SetDTO>() 
                .ForMember(dto => dto.Exercise, opt => opt
                .MapFrom(s => s.Exercise))
                .ForMember(dto => dto.Workouts, opt => opt
                .MapFrom(s => s.Workouts.ToList()));

                CreateMap<SetPutDTO, Set>();
                CreateMap<Set, SetSummaryDTO>();
            
        }
    }
}
