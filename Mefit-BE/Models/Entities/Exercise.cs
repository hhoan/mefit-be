﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mefit_BE.Models.Entities
{
    [Table("Excercise")]
    public class Exercise
    {
        [Key]
        public int Exercise_id { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; } = null!;

        [Required]
        [StringLength(1028)]
        public string Description { get; set; } = null!;

        [ForeignKey("FK_MuscleGroup")]
        public int MuscleGroup_Id { get; set; }

        [StringLength(128)]
        public string? Image { get; set; }

        [StringLength(128)]
        public string? VidLink { get; set; } = null!;

        public MuscleGroup MuscleGroup { get; set; } = null!;
        public ICollection<Set> Sets { get; set; } = new HashSet<Set>();
    }
}
