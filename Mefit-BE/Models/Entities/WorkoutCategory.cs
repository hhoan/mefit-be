﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mefit_BE.Models.Entities
{
    [Table("WorkoutCategory")]
    public class WorkoutCategory
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(32)]
        public string Name { get; set; } = null!;

        public ICollection<Workout> Workouts { get; set; } = new HashSet<Workout>();
    }
}
