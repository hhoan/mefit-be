﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mefit_BE.Models.Entities
{
    [Table("Program")]
    public class FitnessProgram
    {
        [Key]
        public int Program_id { get; set; }

        [Required]
        public string Name { get; set; } = null!;

        public int Category_id { get; set; }

        //Navigation
        public ProgramCategory programCategory { get; set; } = null!;
        public ICollection<Workout> Workouts { get; set; } = new HashSet<Workout>();

        public ICollection<Goal> Goals { get; set; } = new HashSet<Goal>();

    }
}
