﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Mefit_BE.Models.Entities
{
    [Table("Goal")]
    public class Goal
    {
        [Key]
        public int Goal_id { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        public bool Achieved { get; set; }

        [ForeignKey("FK_GoalProgramId")]
        public int? GoalProgram_Id { get; set; }
        public virtual FitnessProgram Program { get; set; } = null!;

        [ForeignKey("FK_GoalUserProfile")]
        public string UserProfile_id { get; set; } = null!;
        public virtual UserProfile Profile { get; set; } = null!;

        public ICollection<Workout> Workouts { get; set; } = new List<Workout>();
    }
}
