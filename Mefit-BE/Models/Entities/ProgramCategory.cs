﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mefit_BE.Models.Entities
{
    [Table("ProgramCatergory")]
    public class ProgramCategory
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(32)]
        public string Name { get; set; } = null!;

        public ICollection<FitnessProgram> FitnessPrograms { get; set; } = new HashSet<FitnessProgram>();
    }
}
