﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mefit_BE.Models.Entities
{
    [Table("MuscleGroup")]
    public class MuscleGroup
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(32)]
        public string Name { get; set; } = null!;

        public virtual ICollection<Exercise> Exercises { get; set; } = new HashSet<Exercise>();
    }
}
