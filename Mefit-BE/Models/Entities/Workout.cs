﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Mefit_BE.Models.Entities
{
    [Table("Workout")]
    public class Workout
    {
        [Key]
        public int Workout_id { get; set; }

        [Required]
        [StringLength(64)]
        public string WorkoutName { get; set; } = null!;

        public bool Complete { get; set; }

        public int WorkoutCategory_id { get; set; }

        //Navigation

        public WorkoutCategory WorkoutCategory { get; set; } = null!;
        public ICollection<Set> Sets { get; set; } = new HashSet<Set>();
        public ICollection<Goal> Goals { get; set; } = new HashSet<Goal>();
        public ICollection<FitnessProgram> Programs { get; set; } = new HashSet<FitnessProgram>();

    }
}
