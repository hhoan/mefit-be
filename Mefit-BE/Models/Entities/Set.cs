﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Mefit_BE.Models.Entities
{
    [Table("Set")]

    public class Set
    {
        [Key]
        public int Set_id { get; set; }

        [Required]
        public int Exercise_repetitions { get; set; }

        [Required]
        public int Set_amount { get; set; }

        //Navigation
        [Required]
        [ForeignKey("Exercise")]
        public int Exercise_id { get; set; }
        public virtual Exercise Exercise { get; set; } = null!;
        public ICollection<Workout> Workouts { get; set; } = new HashSet<Workout>();
    }
}
