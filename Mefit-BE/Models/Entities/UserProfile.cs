﻿using Microsoft.OpenApi.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mefit_BE.Models.Entities
{
    [Table("UserProfile")]
    public class UserProfile
    {
        //[Key]
        //public int UserProfile_id { get; set; }

        [Key]
        [StringLength(64)]
        public string Id { get; set; } 

        public double? Weight { get; set; }  

        public double? Height { get; set; }

        public string? FitnessLevel { get; set; } 

        public string? FitnessGoal { get; set; }

        //[Required]
        //[StringLength(50)]
        //public string FirstName { get; set; } = null!;

        //[Required]
        //[StringLength(50)]
        //public string LastName { get; set; } = null!;

        //[Required]
        //[StringLength(100)]
        //public string Email { get; set; } = null!;

        //public bool IsContributor { get; set; }

        //public bool IsAdmin { get; set; }

        public virtual ICollection<Goal> Goals { get; set; } = new List<Goal>();
    }
}
