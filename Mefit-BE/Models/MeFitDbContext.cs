﻿using Mefit_BE.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Mefit_BE.Models
{
    public class MeFitDbContext : DbContext
    {
        public MeFitDbContext(DbContextOptions<MeFitDbContext> options) : base(options)
        {
        }
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<Set> Sets { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<Goal> Goals { get; set; }
        public DbSet<FitnessProgram> Programs { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<MuscleGroup> MuscleGroups { get; set; }
        public DbSet<ProgramCategory> ProgramCategories { get; set; }
        public DbSet<WorkoutCategory> WorkoutCategories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Set>()
                .HasOne(s => s.Exercise)
                .WithMany(e => e.Sets)
                .HasForeignKey(s => s.Exercise_id);

            modelBuilder.Entity<Workout>()
                .HasMany<Set>(w => w.Sets)
                .WithMany(s => s.Workouts)
                .UsingEntity<Dictionary<string, object>>(
                    "SetWorkout",
                    r => r.HasOne<Set>().WithMany().HasForeignKey("Set_Id"),
                    l => l.HasOne<Workout>().WithMany().HasForeignKey("Workout_Id"),
                   je =>
                   {
                       je.HasKey("Set_Id", "Workout_Id");
                       je.HasData(
                           new { Set_Id = 4, Workout_Id = 1 },
                           new { Set_Id = 5, Workout_Id = 1 },
                           new { Set_Id = 7, Workout_Id = 1 },
                           new { Set_Id = 8, Workout_Id = 1 },
                           new { Set_Id = 2, Workout_Id = 2 },
                           new { Set_Id = 9, Workout_Id = 2 },
                           new { Set_Id = 15, Workout_Id = 2 },
                           new { Set_Id = 17, Workout_Id = 2 },
                           new { Set_Id = 1, Workout_Id = 3 },
                           new { Set_Id = 2, Workout_Id = 3 },
                           new { Set_Id = 3, Workout_Id = 3 },
                           new { Set_Id = 9, Workout_Id = 3 },
                           new { Set_Id = 1, Workout_Id = 4 },
                           new { Set_Id = 7, Workout_Id = 4 },
                           new { Set_Id = 8, Workout_Id = 4 },
                           new { Set_Id = 11, Workout_Id = 4 },
                           new { Set_Id = 2, Workout_Id = 5 },
                           new { Set_Id = 3, Workout_Id = 5 },
                           new { Set_Id = 6, Workout_Id = 5 },
                           new { Set_Id = 14, Workout_Id = 5 },
                           new { Set_Id = 3, Workout_Id = 6 },
                           new { Set_Id = 6, Workout_Id = 6 },
                           new { Set_Id = 7, Workout_Id = 6 },
                           new { Set_Id = 16, Workout_Id = 6 },
                           new { Set_Id = 3, Workout_Id = 7 },
                           new { Set_Id = 9, Workout_Id = 7 },
                           new { Set_Id = 15, Workout_Id = 7 },
                           new { Set_Id = 16, Workout_Id = 7 },
                           new { Set_Id = 10, Workout_Id = 8 },
                           new { Set_Id = 12, Workout_Id = 8 },
                           new { Set_Id = 13, Workout_Id = 8 },
                           new { Set_Id = 14, Workout_Id = 8 },
                           new { Set_Id = 2, Workout_Id = 9 },
                           new { Set_Id = 7, Workout_Id = 9 },
                           new { Set_Id = 9, Workout_Id = 9 },
                           new { Set_Id = 16, Workout_Id = 9 }
                       );
                   });


            modelBuilder.Entity<Workout>()
               .HasMany<FitnessProgram>(w => w.Programs)
               .WithMany(p => p.Workouts)
               .UsingEntity<Dictionary<string, object>>(
                    "WorkoutProgram",
                    r => r.HasOne<FitnessProgram>().WithMany().HasForeignKey("Program_Id"),
                    l => l.HasOne<Workout>().WithMany().HasForeignKey("Workout_Id"),
                    je =>
                    {
                        je.HasKey("Program_Id", "Workout_Id");
                        je.HasData(
                            new { Program_Id = 1, Workout_Id = 3 },
                            new { Program_Id = 1, Workout_Id = 4 },
                            new { Program_Id = 1, Workout_Id = 7 },
                            new { Program_Id = 2, Workout_Id = 1 },
                            new { Program_Id = 2, Workout_Id = 2 },
                            new { Program_Id = 2, Workout_Id = 5 },
                            new { Program_Id = 3, Workout_Id = 3 },
                            new { Program_Id = 3, Workout_Id = 6 },
                            new { Program_Id = 3, Workout_Id = 7 },
                            new { Program_Id = 4, Workout_Id = 6 },
                            new { Program_Id = 4, Workout_Id = 8 },
                            new { Program_Id = 4, Workout_Id = 9 }
                        );
                    });



            modelBuilder.Entity<Goal>()
                .HasMany(G => G.Workouts)
                .WithMany(W => W.Goals)
                .UsingEntity<Dictionary<string, object>>(
                    "GoalWorkout",
                    r => r.HasOne<Workout>().WithMany().HasForeignKey("Workout_Id"),
                    l => l.HasOne<Goal>().WithMany().HasForeignKey("Goal_Id"));

            modelBuilder.Entity<Goal>()
                .HasOne(G => G.Program)
                .WithMany(P => P.Goals)
                .HasForeignKey(G => G.GoalProgram_Id);


            //modelBuilder.Entity<UserProfile>()
            //    .HasMany(p => p.Goals)
            //    .WithOne(g => g.Profile)
            //    .HasForeignKey(p => p.Goal_id);

            modelBuilder.Entity<Goal>()
                .HasOne(g => g.Profile)
                .WithMany(p => p.Goals)
                .HasForeignKey(g => g.UserProfile_id);

            modelBuilder.Entity<Exercise>()
                .HasOne(e => e.MuscleGroup)
                .WithMany(m => m.Exercises)
                .HasForeignKey(e => e.MuscleGroup_Id);

            modelBuilder.Entity<Workout>()
                .HasOne(w => w.WorkoutCategory)
                .WithMany(c => c.Workouts)
                .HasForeignKey(w => w.WorkoutCategory_id);

            modelBuilder.Entity<FitnessProgram>()
                .HasOne(f => f.programCategory)
                .WithMany(c => c.FitnessPrograms)
                .HasForeignKey(f => f.Category_id);


           /* SEED DATA */

            modelBuilder.Entity<UserProfile>().HasData(
                new UserProfile
                {
                    Id = "d7a65416-a8eb-40ac-beeb-f32e91e3de9d",//role: admin
                    Weight = 70.5,
                    Height = 170,
                    FitnessLevel = "Intermediate",
                    FitnessGoal = "Muscle gain"
                },
                new UserProfile
                {
                    Id = "3bb30c7b-2b03-4efa-a108-de0fb92b92bd",//role: contributor
                    Weight = 60.2,
                    Height = 165,
                    FitnessLevel = "Beginner",
                    FitnessGoal = "Weight loss"
                },
                new UserProfile
                {
                    Id = "3846e120-6814-4140-9bd4-c6b53d989582",//role: user
                    Weight = 80.1,
                    Height = 180,
                    FitnessLevel = "Advanced",
                    FitnessGoal = "Maintain weight"
                });

            // Goal entities
            modelBuilder.Entity<Goal>().HasData(
                new Goal { Goal_id = 1, UserProfile_id = "d7a65416-a8eb-40ac-beeb-f32e91e3de9d", EndDate = new DateTime(2023, 12, 31), Achieved = false, GoalProgram_Id = 1 },
                new Goal { Goal_id = 2, UserProfile_id = "3bb30c7b-2b03-4efa-a108-de0fb92b92bd", EndDate = new DateTime(2024, 6, 30), Achieved = false, GoalProgram_Id = 2 },
                new Goal { Goal_id = 3, UserProfile_id = "3846e120-6814-4140-9bd4-c6b53d989582", EndDate = new DateTime(2023, 9, 30), Achieved = true, GoalProgram_Id = 3 }
            );

            // MuscleGroup entities
            modelBuilder.Entity<MuscleGroup>().HasData(
                new MuscleGroup { Id = 1, Name = "Chest" },
                new MuscleGroup { Id = 2, Name = "Legs" },
                new MuscleGroup { Id = 3, Name = "Back" },
                new MuscleGroup { Id = 4, Name = "Arms" },
                new MuscleGroup { Id = 5, Name = "Core" },
                new MuscleGroup { Id = 6, Name = "Shoulder" }
            );

            // ProgramCategory entities
            modelBuilder.Entity<ProgramCategory>().HasData(
                new ProgramCategory { Id = 1, Name = "Strength" },
                new ProgramCategory { Id = 2, Name = "Cardiovascular" },
                new ProgramCategory { Id = 3, Name = "Endurance" },
                new ProgramCategory { Id = 4, Name = "Strength and Cardio" }
            );

            // Set entities
            modelBuilder.Entity<Set>().HasData(
                new Set { Set_id = 1, Exercise_repetitions = 10, Set_amount = 3, Exercise_id = 1 },
                new Set { Set_id = 2, Exercise_repetitions = 8, Set_amount = 4, Exercise_id = 2 },
                new Set { Set_id = 3, Exercise_repetitions = 12, Set_amount = 2, Exercise_id = 3 },
                new Set { Set_id = 4, Exercise_repetitions = 10, Set_amount = 3, Exercise_id = 4 },
                new Set { Set_id = 5, Exercise_repetitions = 8, Set_amount = 4, Exercise_id = 5 },
                new Set { Set_id = 6, Exercise_repetitions = 12, Set_amount = 2, Exercise_id = 6 },
                new Set { Set_id = 7, Exercise_repetitions = 10, Set_amount = 3, Exercise_id = 7 },
                new Set { Set_id = 8, Exercise_repetitions = 8, Set_amount = 4, Exercise_id = 8 },
                new Set { Set_id = 9, Exercise_repetitions = 15, Set_amount = 10, Exercise_id = 9 },
                new Set { Set_id = 10, Exercise_repetitions = 1, Set_amount = 5, Exercise_id = 10 },
                new Set { Set_id = 11, Exercise_repetitions = 12, Set_amount = 2, Exercise_id = 11 },
                new Set { Set_id = 12, Exercise_repetitions = 1, Set_amount = 5, Exercise_id = 12 },
                new Set { Set_id = 13, Exercise_repetitions = 1, Set_amount = 5, Exercise_id = 13 },
                new Set { Set_id = 14, Exercise_repetitions = 1, Set_amount = 5, Exercise_id = 14 },
                new Set { Set_id = 15, Exercise_repetitions = 10, Set_amount = 3, Exercise_id = 15 },
                new Set { Set_id = 16, Exercise_repetitions = 15, Set_amount = 5, Exercise_id = 16 },
                new Set { Set_id = 17, Exercise_repetitions = 12, Set_amount = 3, Exercise_id = 17 }
            );

            // Exercise entities
            modelBuilder.Entity<Exercise>().HasData(
                new Exercise { Exercise_id = 1, Name = "Bench Press", Description = "Lie on your back on a bench, grip a barbell with both hands and lower it to your chest, then push it back up to straight arms. This exercise primarily targets the chest muscles and also works the shoulders and triceps.", MuscleGroup_Id = 1 },
                new Exercise { Exercise_id = 2, Name = "Squats", Description = "Stand with your feet shoulder-width apart, hold a barbell on your shoulders, and lower yourself until your thighs are parallel to the ground, then push back up to standing. This exercise targets the lower body muscles, including the quadriceps, hamstrings, and glutes.", MuscleGroup_Id = 2 },
                new Exercise { Exercise_id = 3, Name = "Deadlifts", Description = "Stand with your feet shoulder-width apart, grip a barbell with both hands, then lift it off the ground by extending your hips and knees until you're standing straight up. This exercise primarily targets the back muscles and also works the legs and grip strength.", MuscleGroup_Id = 3 },
                new Exercise { Exercise_id = 4, Name = "Bicep curl", Description = "Hold a dumbbell in each hand with your palms facing up, then bend your arms to bring the weights towards your shoulders, and lower them back down. This exercise targets the biceps.", MuscleGroup_Id = 4 },
                new Exercise { Exercise_id = 5, Name = "Tricep curl", Description = "Hold a dumbbell in each hand with your palms facing down, then extend your arms straight back behind you, and lower them back down. This exercise targets the triceps.", MuscleGroup_Id = 4 },
                new Exercise { Exercise_id = 6, Name = "Plank", Description = "Get into a pushup position but instead of lowering down, hold yourself in a straight line from head to heels, with your elbows and forearms on the ground. This exercise targets the core muscles, including the abdominals and lower back.", MuscleGroup_Id = 5 },
                new Exercise { Exercise_id = 7, Name = "Pushup", Description = "Get into a plank position with your hands on the ground, then lower your body down to the ground and push back up to straight arms. This exercise targets the chest, shoulders, and triceps.", MuscleGroup_Id = 4 },
                new Exercise { Exercise_id = 8, Name = "Pullup", Description = "Grip a bar overhead with your hands facing away from you, then pull your body up until your chin is above the bar, then lower back down. This exercise targets the back muscles and also works the arms and shoulders.", MuscleGroup_Id = 4 },
                new Exercise { Exercise_id = 9, Name = "Stair running", Description = "Stair running, involves running up and down stairs as a form of cardiovascular exercise that targets the lower body, particularly the quadriceps, hamstrings, and glutes. This exercise can be performed outdoors or indoors, and can be modified to increase or decrease intensity by adjusting the speed and number of stairs climbed.", MuscleGroup_Id = 2 },
                new Exercise { Exercise_id = 10, Name = "Downward dog", Description = "Downward dog, is a yoga pose that involves starting on all fours, then lifting the hips and straightening the legs to form an inverted V-shape with the body. This pose targets the hamstrings, calves, and shoulders, and can help to improve flexibility, balance, and posture.", MuscleGroup_Id = 5 },
                new Exercise { Exercise_id = 11, Name = "Shoulder press", Description = "Shoulder press, involves lifting weights overhead while seated or standing to target the deltoids, triceps, and upper back muscles. This exercise can be performed with dumbbells, barbells, or resistance bands, and can help to improve upper body strength and posture.", MuscleGroup_Id = 6 },
                new Exercise { Exercise_id = 12, Name = "Upward dog", Description = "Upward dog, is a yoga pose that involves starting on the stomach, then lifting the chest and upper body while keeping the legs and lower body on the ground. This pose targets the chest, shoulders, and back, and can help to improve flexibility and posture.", MuscleGroup_Id = 1 },
                new Exercise { Exercise_id = 13, Name = "Warrior one", Description = "Warrior one, is a yoga pose that involves starting in a lunge position with the back foot turned out, then lifting the arms overhead and squaring the hips to the front. This pose targets the quadriceps, hamstrings, and glutes, and can help to improve balance and lower body strength.", MuscleGroup_Id = 2 },
                new Exercise { Exercise_id = 14, Name = "Crow pose", Description = "Crow pose is a yoga pose. To perform the Crow pose, start in a squat position and place your hands on the ground in front of you. Then, lift your feet off the ground and balance on your hands while keeping your knees on your arms. This yoga pose primarily targets the arm and shoulder muscles, such as the triceps, biceps, and deltoids, as well as the wrists and core muscles.", MuscleGroup_Id = 4 },
                new Exercise { Exercise_id = 15, Name = "Squat jumping", Description = "To perform Squat jumping, start in a squat position and then jump as high as you can, using your leg muscles to power the movement. This plyometric exercise primarily targets the leg muscles, including the quadriceps, hamstrings, and calves, as well as the glutes and core muscles for stability and power.", MuscleGroup_Id = 2 },
                new Exercise { Exercise_id = 16, Name = "Interval running", Description = "To perform interval running, alternate periods of high-intensity running with periods of lower-intensity recovery. This type of cardio workout targets the leg muscles, including the quadriceps, hamstrings, calves, and glutes, as well as the core muscles, and can improve cardiovascular endurance.\r\n", MuscleGroup_Id = 2 },
                new Exercise { Exercise_id = 17, Name = "Leg curl", Description = "To perform leg curls, lie face down on a machine with your legs hooked under a padded bar, then curl your legs up towards your buttocks by contracting your hamstrings. This exercise specifically targets the hamstring muscles, which are the muscles on the back of your thighs.", MuscleGroup_Id = 2 }

            );



            // Workout entities
            modelBuilder.Entity<Workout>().HasData(
                new Workout { Workout_id = 1, WorkoutName = "Upper Body Workout", Complete = false, WorkoutCategory_id = 1 },
                new Workout { Workout_id = 2, WorkoutName = "Lower Body Workout", Complete = false, WorkoutCategory_id = 1 },
                new Workout { Workout_id = 3, WorkoutName = "Explosive Full Body Workout", Complete = false, WorkoutCategory_id = 3 },
                new Workout { Workout_id = 4, WorkoutName = "Explosive Upper Body Workout", Complete = false, WorkoutCategory_id = 1 },
                new Workout { Workout_id = 5, WorkoutName = "Core Workout", Complete = false, WorkoutCategory_id = 1 },
                new Workout { Workout_id = 6, WorkoutName = "Endurance Core Workout", Complete = false, WorkoutCategory_id = 3 },
                new Workout { Workout_id = 7, WorkoutName = "Cardivascular Health Workout", Complete = false, WorkoutCategory_id = 1 },
                new Workout { Workout_id = 8, WorkoutName = "Vinyasa Yoga Workout", Complete = false, WorkoutCategory_id = 1 },
                new Workout { Workout_id = 9, WorkoutName = "Endurance Cardivascular Workout", Complete = false, WorkoutCategory_id = 3 }
            );


            // FitnessProgram entities
            modelBuilder.Entity<FitnessProgram>().HasData(
                new FitnessProgram { Program_id = 1, Name = "Explosive Training Program", Category_id = 2 },
                new FitnessProgram { Program_id = 2, Name = "Strength Program", Category_id = 1 },
                new FitnessProgram { Program_id = 3, Name = "HIIT Program", Category_id = 4 },
                new FitnessProgram { Program_id = 4, Name = "Endurance Program", Category_id = 3 }
            );

            // WorkoutCategory entities
            modelBuilder.Entity<WorkoutCategory>().HasData(
                new WorkoutCategory
                {
                    Id = 1,
                    Name = "Strength Training"
                },
                new WorkoutCategory
                {
                    Id = 2,
                    Name = "Cardiovascular Exercise"
                },
                new WorkoutCategory
                {
                    Id = 3,
                    Name = "Yoga"
                }
            );
        }


    }
}
