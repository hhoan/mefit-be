﻿using System.ComponentModel.DataAnnotations;

namespace Mefit_BE.Models.DTOs.WorkoutCategories
{
    public class WorkoutCategoryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
    }
}
