﻿namespace Mefit_BE.Models.DTOs.WorkoutCategories
{
    public class WorkoutCategoryPutDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
    }
}
