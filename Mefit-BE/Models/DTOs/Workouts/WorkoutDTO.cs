﻿using Mefit_BE.Models.DTOs.Sets;
using Mefit_BE.Models.DTOs.WorkoutCategories;
using Mefit_BE.Models.Entities;
using System.ComponentModel.DataAnnotations;

namespace Mefit_BE.Models.DTOs.Workouts
{
    public class WorkoutDTO
    {
        public int Workout_id { get; set; }

        public string WorkoutName { get; set; } = null!;

        public bool Complete { get; set; }

        public WorkoutCategoryDTO WorkoutCategory { get; set; } = null!;
        public List<SetSummaryDTO> Sets { get; set; } = new List<SetSummaryDTO>();
    }
}
