﻿using System.ComponentModel.DataAnnotations;

namespace Mefit_BE.Models.DTOs.Workouts
{
    public class WorkoutSummaryDTO
    {
        public int Workout_id { get; set; }

        public string WorkoutName { get; set; } = null!;

        public bool Complete { get; set; }

        public int WorkoutCategory_id { get; set; }
    }
}
