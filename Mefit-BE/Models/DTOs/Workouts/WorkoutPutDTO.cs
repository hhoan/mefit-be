﻿using Mefit_BE.Models.DTOs.Sets;
using Mefit_BE.Models.DTOs.WorkoutCategories;
using Mefit_BE.Models.Entities;

namespace Mefit_BE.Models.DTOs.Workouts
{
    public class WorkoutPutDTO
    {
        public int Workout_id { get; set; }
        public string WorkoutName { get; set; } = null!;
        public bool Complete { get; set; }
        public int WorkoutCategory_id { get; set; }
    }
}
