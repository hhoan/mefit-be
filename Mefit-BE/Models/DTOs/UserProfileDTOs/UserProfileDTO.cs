﻿using Mefit_BE.Models.DTOs.GoalDTOs;
using Mefit_BE.Models.Entities;
using System.ComponentModel.DataAnnotations;

namespace Mefit_BE.Models.DTOs.UserProfileDTOs
{
    public class UserProfileDTO
    {
        public string Id { get; set; }
        public double? Weight { get; set; }
        public double? Height { get; set; }
        public string? FitnessLevel { get; set; }
        public string? FitnessGoal { get; set; }
        public virtual List<GoalSummaryDTO>? Goals { get; set; } = null!;
    }
}
