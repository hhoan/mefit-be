﻿using Mefit_BE.Models.DTOs.GoalDTOs;
using System.ComponentModel.DataAnnotations;

namespace Mefit_BE.Models.DTOs.UserProfileDTOs
{
    public class UserProfilePostDTO
    {
        public string Id { get; set; }
        public double? Weight { get; set; }

        public double? Height { get; set; }

        public string? FitnessLevel { get; set; }

        public string? FitnessGoal { get; set; }

    }
}
