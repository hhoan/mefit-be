﻿using Mefit_BE.Models.DTOs.GoalDTOs;

namespace Mefit_BE.Models.DTOs.UserProfileDTOs
{
    public class UserProfilePutDTO
    {
        public string Id { get; set; }
        public double Weight { get; set; }

        public double Height { get; set; }

        public string FitnessLevel { get; set; } = null!;

        public string FitnessGoal { get; set; } = null!;

    }
}
