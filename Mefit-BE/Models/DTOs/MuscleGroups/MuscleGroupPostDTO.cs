﻿namespace Mefit_BE.Models.DTOs.MuscleGroups
{
    public class MuscleGroupPostDTO
    {
        public string? Name { get; set; } = null!;
    }
}
