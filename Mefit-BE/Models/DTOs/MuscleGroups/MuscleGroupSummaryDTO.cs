﻿namespace Mefit_BE.Models.DTOs.MuscleGroups
{
    public class MuscleGroupSummaryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
    }
}
