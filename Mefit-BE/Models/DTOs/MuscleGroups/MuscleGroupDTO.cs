﻿using Mefit_BE.Models.Entities;
using System.ComponentModel.DataAnnotations;
using Mefit_BE.Models.DTOs.Exercises;

namespace Mefit_BE.Models.DTOs.MuscleGroups
{
    public class MuscleGroupDTO
    {
        public int Id { get; set; }

        public string Name { get; set; } = null!;

        public List<int> Exercises { get; set; } = null!;
    }
}
