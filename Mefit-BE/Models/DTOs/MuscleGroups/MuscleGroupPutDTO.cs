﻿namespace Mefit_BE.Models.DTOs.MuscleGroups
{
    public class MuscleGroupPutDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
    }
}
