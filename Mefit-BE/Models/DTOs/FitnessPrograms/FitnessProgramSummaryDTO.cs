﻿using Mefit_BE.Models.DTOs.Workouts;

namespace Mefit_BE.Models.DTOs.FitnessPrograms
{
    public class FitnessProgramSummaryDTO
    {
        public int Program_id { get; set; }

        public string Name { get; set; } = null!;

        public int Category_id { get; set; }

        public List<WorkoutSummaryDTO> Workouts { get; set; } = new List<WorkoutSummaryDTO>();
    }
}
