﻿using Mefit_BE.Models.DTOs.ProgramCategories;
using Mefit_BE.Models.DTOs.WorkoutCategories;
using Mefit_BE.Models.DTOs.Workouts;
using Mefit_BE.Models.Entities;

namespace Mefit_BE.Models.DTOs.FitnessPrograms
{
    public class FitnessProgramDTO
    {
        public int Program_id { get; set; }

        public string Name { get; set; } = null!;

        public int Category_id { get; set; }

        public ProgramCategoryDTO programCategory { get; set; } = null!;
        public List<WorkoutSummaryDTO> Workouts { get; set; } = new List<WorkoutSummaryDTO>();
    }
}
