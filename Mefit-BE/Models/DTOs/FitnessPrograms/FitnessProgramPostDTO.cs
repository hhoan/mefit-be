﻿using Mefit_BE.Models.Entities;

namespace Mefit_BE.Models.DTOs.FitnessPrograms
{
    public class FitnessProgramPostDTO
    {
        public string Name { get; set; } = null!;

        public int Category_id { get; set; }

    }
}
