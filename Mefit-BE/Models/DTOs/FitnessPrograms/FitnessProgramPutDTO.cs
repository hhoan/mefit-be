﻿using Mefit_BE.Models.Entities;

namespace Mefit_BE.Models.DTOs.FitnessPrograms
{
    public class FitnessProgramPutDTO
    {
        public int Program_id { get; set; }

        public string Name { get; set; } = null!;

        public int Category_id { get; set; }
    }
}
