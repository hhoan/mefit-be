﻿using Mefit_BE.Models.DTOs.FitnessPrograms;
using Mefit_BE.Models.DTOs.Workouts;
using Mefit_BE.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mefit_BE.Models.DTOs.GoalDTOs
{
    public class GoalDTO
    {
        public int Goal_id { get; set; }

        public DateTime EndDate { get; set; }
        public bool Achieved { get; set; }
        public string UserProfile_id { get; set; } = null!;
        public virtual FitnessProgramSummaryDTO Program { get; set; } = null!;

        //public ICollection<WorkoutSummaryDTO> Workouts { get; set; } = new List<WorkoutSummaryDTO>();
    }
}
