﻿using Mefit_BE.Models.DTOs.FitnessPrograms;
using Mefit_BE.Models.Entities;

namespace Mefit_BE.Models.DTOs.GoalDTOs
{
    public class GoalSummaryDTO
    {
        public int Goal_id { get; set; }
        public DateTime EndDate { get; set; }
        public bool Achieved { get; set; }
        public virtual FitnessProgramDTO Program { get; set; } = null!;
    }
}
