﻿namespace Mefit_BE.Models.DTOs.GoalDTOs
{
    public class GoalPostDTO
    {
        public string UserProfile_id { get; set; } = null!;
        public DateTime EndDate { get; set; }
        public int? GoalProgram_Id { get; set; }
    }
}
