﻿namespace Mefit_BE.Models.DTOs.GoalDTOs
{
    public class GoalPutDTO
    {
        public int Goal_id { get; set; }

        public string UserProfile_id { get; set; } = null!;

        public DateTime EndDate { get; set; }
        public bool Achieved { get; set; }

        public int? GoalProgram_Id { get; set; }
    }
}
