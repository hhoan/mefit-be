﻿namespace Mefit_BE.Models.DTOs.ProgramCategories
{
    public class ProgramCategoryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
    }
}
