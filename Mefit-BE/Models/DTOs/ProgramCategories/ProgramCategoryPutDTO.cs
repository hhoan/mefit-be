﻿namespace Mefit_BE.Models.DTOs.ProgramCategories
{
    public class ProgramCategoryPutDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
    }
}
