﻿using Mefit_BE.Models.DTOs.Exercises;
using Mefit_BE.Models.DTOs.Workouts;
using Mefit_BE.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mefit_BE.Models.DTOs.Sets
{
    public class SetDTO
    {
        public int Set_id { get; set; }
        public int Exercise_repetitions { get; set; }
        public int Set_amount { get; set; }
        public virtual ExerciseSummaryDTO Exercise { get; set; } = null!;
        public List<WorkoutSummaryDTO> Workouts { get; set; } = null!;
    }
}
