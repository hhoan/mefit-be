﻿using Mefit_BE.Models.Entities;

namespace Mefit_BE.Models.DTOs.Sets
{
    public class SetPutDTO
    {
        public int Set_id { get; set; }
        public int Exercise_repetitions { get; set; }
        public int Set_amount { get; set; }
        public int Exercise_id { get; set; }
        
        // public virtual Exercise Exercise { get; set; } = null!;
        // public List<Workout> Workouts { get; set; } = null!;
    }
}
