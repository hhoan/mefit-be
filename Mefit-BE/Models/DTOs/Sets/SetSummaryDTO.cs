﻿namespace Mefit_BE.Models.DTOs.Sets
{
    public class SetSummaryDTO
    {
        public int Set_id { get; set; }
        public int Exercise_repetitions { get; set; }
        public int Set_amount { get; set; }
        public int Exercise_id { get; set; }
    }
}
