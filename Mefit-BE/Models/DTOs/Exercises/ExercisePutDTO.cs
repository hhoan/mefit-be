﻿namespace Mefit_BE.Models.DTOs.Exercises
{
    public class ExercisePutDTO
    {
        public int Exercise_id { get; set; }

        public string Name { get; set; } = null!;

        public string Description { get; set; } = null!;

        public int MuscleGroup_Id { get; set; }

        public string Image { get; set; } = null!;

    }
}
