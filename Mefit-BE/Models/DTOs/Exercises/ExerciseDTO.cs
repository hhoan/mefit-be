﻿using Mefit_BE.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Mefit_BE.Models.DTOs.Sets;

namespace Mefit_BE.Models.DTOs.Exercises
{
    public class ExerciseDTO
    {
        public int Exercise_id { get; set; }

        public string Name { get; set; } = null!;

        public string Description { get; set; } = null!;

        public int MuscleGroup_Id { get; set; }

        public string Image { get; set; } = null!;

        //public string VidLink { get; set; } = null!;

        // public MuscleGroup MuscleGroup { get; set; } = null!;

        public List<SetSummaryDTO>? Sets { get; set; } = new List<SetSummaryDTO>();
    }
}
