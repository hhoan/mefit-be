﻿namespace Mefit_BE.Models.DTOs.Exercises
{
    public class ExerciseSummaryDTO
    {
            public int Exercise_id { get; set; }
            public string Name { get; set; } = null!;
    }
}
