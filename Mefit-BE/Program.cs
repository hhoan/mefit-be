using Mefit_BE.Models;
using Mefit_BE.Services.WorkoutCategoryServices;
using Mefit_BE.Services.Exercises;
using Mefit_BE.Services.Sets;
using Mefit_BE.Services.UserProfileService;
using Mefit_BE.Services.MuscleGroups;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using Mefit_BE.Services.ProgramCategoryServices;
using Mefit_BE.Services.FitnessPrograms;
using Mefit_BE.Services.WorkoutServices;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using static System.Net.WebRequestMethods;
using Newtonsoft.Json;
using Mefit_BE.Services.GoalServices;
using Microsoft.OpenApi.Models;

namespace Mefit_BE
{
    public class Program
    {
        public static void Main(string[] args) 
        {
            var builder = WebApplication.CreateBuilder(args);
             
             

            // Add services to the container.

            builder.Services.AddDbContext<MeFitDbContext>(options
                => options.UseSqlServer(builder.Configuration.GetConnectionString("MyConnectionString")));
            
            builder.Services.AddTransient<IMuscleGroupService, MuscleGroupServiceImpl>();
            builder.Services.AddTransient<IExerciseService, ExerciseServiceImpl>();

            builder.Services.AddTransient<IUserProfileService, UserProfileService>();
            builder.Services.AddTransient<IGoalService, GoalService>();
            builder.Services.AddTransient<IWorkoutCategoryService, WorkoutCategoryService>();
            builder.Services.AddTransient<IWorkoutService, WorkoutService>();
            builder.Services.AddTransient<IProgramCategoryService, ProgramCategoryService>();
            builder.Services.AddTransient<ISetService, SetServiceImpl>();
            builder.Services.AddTransient<IFitnessProgramService, FitnessProgramService>();


            //builder.Services.AddControllers();
            builder.Services.AddControllers().AddJsonOptions(x =>
               x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();

            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "MeFit API",
                    Description = "An ASP.NET Core Web API for managing MeFit database",
                });
                var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
            });


            builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => 
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = true,
                        ValidateIssuer = true,

                        ValidAudience = builder.Configuration["JWT:audience"],
                        ValidIssuer = builder.Configuration["JWT:Issuer"],
                        IssuerSigningKeyResolver = (token, securityToken, kid, parameters) =>
                        {
                            var client = new HttpClient();
                            var keyUri = builder.Configuration["JWT:KeyUri"];

                            // Fetch certificate keys from keycloak instance
                            var response = client.GetAsync(keyUri).Result;
                            var responseString = response.Content.ReadAsStringAsync().Result;
                            var keys = JsonConvert.DeserializeObject<JsonWebKeySet>(responseString);
                            return keys?.Keys;
                        }
                    };
                }
                
                );

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}