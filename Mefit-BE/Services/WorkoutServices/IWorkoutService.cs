﻿using Mefit_BE.Models.Entities;

namespace Mefit_BE.Services.WorkoutServices
{
    public interface IWorkoutService : ICrudService<Workout, int>
    {
        public Task<bool> workoutExists(int id);
        public Task UpdateSetsAsync(int[] SetId, int id);
        Task<ICollection<Exercise>> GetExercisesAsync(int id); //include sets
    }
}
