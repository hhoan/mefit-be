﻿using Mefit_BE.Models;
using Mefit_BE.Models.Entities;
using Mefit_BE.Utils;
using Microsoft.EntityFrameworkCore;

namespace Mefit_BE.Services.WorkoutServices
{
    public class WorkoutService : IWorkoutService
    {
        private readonly MeFitDbContext _context;
        private readonly ILogger<WorkoutService> _logger;

        public WorkoutService(MeFitDbContext context, ILogger<WorkoutService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Workout> AddAsync(Workout entity)
        {
            await _context.Workouts.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            if (!await workoutExists(id))
            {
                _logger.LogError("Workout not found with Id: " + id);
                // throw new notfoundexception();
            }
            var workout = await _context.Workouts.FindAsync(id);
            _context.Workouts.Remove(workout);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Workout>> GetAllAsync()
        {
            return await _context.Workouts
               .Include(w => w.WorkoutCategory)
               .Include(w => w.Sets)
               .ToListAsync();
        }

        public async Task<Workout> GetByIdAsync(int id)
        {
            if (!await workoutExists(id))
            {
                _logger.LogError("Fitness program not found with Id: " + id);
                // throw new notfoundexception();
            }

            return await _context.Workouts
                .Where(w => w.Workout_id == id)
                .Include(w => w.WorkoutCategory)
                .Include(w => w.Sets)
                .FirstAsync();
        }

        public async Task UpdateAsync(Workout entity)
        {
            if (!await workoutExists(entity.Workout_id))
            {
                _logger.LogError("Workout not found with Id: " + entity.Workout_id);
                // throw new notfoundexception();
            }
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateSetsAsync(int[] SetId, int id)
        {
            if (!await workoutExists(id))
            {
                _logger.LogError("Workout not found with Id: " + id);
            }

            List<Set> sets = SetId.ToList()
                .Select(sId => _context.Sets
                .Where(s => s.Set_id == sId).First()).ToList();

            Workout workout = await _context.Workouts
                .Where(w => w.Workout_id == id)
                .FirstAsync();
            workout.Sets = sets;
            _context.Entry(workout).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Exercise>> GetExercisesAsync(int id)
        {
            if (!await workoutExists(id))
            {
                throw new EntityNotFoundException();
            }
            Workout workout = await _context.Workouts
                .Where(w => w.Workout_id == id)
                .Include(w => w.Sets)
                .ThenInclude(s => s.Exercise)
                .FirstAsync();
            ICollection<Exercise> exercises = new List<Exercise>();
            foreach (var set in workout.Sets)
            {
                if (!exercises.Contains(set.Exercise))
                {
                    exercises.Add(set.Exercise);
                }
            }
            return exercises;

        }


        public async Task<bool> workoutExists(int id)
        {
            return await _context.Workouts.AnyAsync(w => w.Workout_id == id);
        }
    }
}
