﻿using Mefit_BE.Models.Entities;
using Mefit_BE.Models;
using Mefit_BE.Services.MuscleGroups;
using Microsoft.EntityFrameworkCore;

namespace Mefit_BE.Services.Exercises
{
    public class ExerciseServiceImpl : IExerciseService
    {
        /// <summary>
        /// Class to house business logic
        /// Interacts with entity framework
        /// Service will be injectable and used in Controller
        /// Context refers to database
        /// </summary>
        /// 

        private readonly MeFitDbContext _context;
        private readonly ILogger<ExerciseServiceImpl> _logger;

        public ExerciseServiceImpl(MeFitDbContext postgradEf, ILogger<ExerciseServiceImpl> logger)
        {
            _context = postgradEf;
            _logger = logger;
        }

        // get all
        public async Task<ICollection<Exercise>> GetAllAsync()
        {
            return await _context.Exercises
                .Include(e => e.Sets)
                .Include(e => e.MuscleGroup)
                .ToListAsync();
        }

        // GET by id
        public async Task<Exercise> GetByIdAsync(int id)
        {
            if (!await ExerciseExistsAsync(id))
            {
                _logger.LogError("Exercise not found with Id: " + id);
                // throw new notfoundexception();
            }

            return await _context.Exercises
                .Where(e => e.Exercise_id == id)
                .Include(e => e.MuscleGroup)
                .Include(e => e.Sets)
                .FirstAsync();
        }

        // post
        public async Task<Exercise> AddAsync(Exercise entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        // update
        public async Task UpdateAsync(Exercise entity)
        {
            if (!await ExerciseExistsAsync(entity.Exercise_id))
            {
                _logger.LogError("Exercise not found with Id: " + entity.Exercise_id);
            }
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        // delete 
        public async Task DeleteAsync(int id)
        {
            var exercise = await _context.Exercises.FindAsync(id);

            if (exercise == null)
            {
                _logger.LogError("Exercise not found with Id: " + id);
            }

            _context.Exercises.Remove(exercise);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> ExerciseExistsAsync(int id)
        {
            return await _context.Exercises.AnyAsync(e => e.Exercise_id == id);
        }
    }
}
