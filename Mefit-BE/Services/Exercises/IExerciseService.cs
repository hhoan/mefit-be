﻿using Mefit_BE.Models.Entities;

namespace Mefit_BE.Services.Exercises
{
    public interface IExerciseService : ICrudService<Exercise, int>
    { 
            /// <summary> 
            /// Check if the muscle group exists by id.
            /// <param name="id">
            /// </summary>
            public Task<bool> ExerciseExistsAsync(int id);
    }
}
