﻿using Mefit_BE.Models;
using Mefit_BE.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Mefit_BE.Services.ProgramCategoryServices
{
    public class ProgramCategoryService : IProgramCategoryService
    {
        private readonly MeFitDbContext _context;
        private readonly ILogger<ProgramCategoryService> _logger;

        public ProgramCategoryService(MeFitDbContext context, ILogger<ProgramCategoryService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<ProgramCategory> AddAsync(ProgramCategory entity)
        {
            await _context.ProgramCategories.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            if (!await programCategoryExists(id))
            {
                _logger.LogError("Program Category not found with Id: " + id);
                // throw new notfoundexception();
            }
            var programCategory = await _context.ProgramCategories.FindAsync(id);
            _context.ProgramCategories.Remove(programCategory);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<ProgramCategory>> GetAllAsync()
        {
            return await _context.ProgramCategories.ToListAsync();
        }

        public async Task<ProgramCategory> GetByIdAsync(int id)
        {
            if (!await programCategoryExists(id))
            {
                _logger.LogError("Program Category not found with Id: " + id);
                // throw new notfoundexception();
            }
            return await _context.ProgramCategories.FindAsync(id);
        }

        public async Task<bool> programCategoryExists(int id)
        {
            return await _context.ProgramCategories.AnyAsync(m => m.Id == id);
        }

        public async Task UpdateAsync(ProgramCategory entity)
        {
            if (!await programCategoryExists(entity.Id))
            {
                _logger.LogError("Program Category not found with Id: " + entity.Id);
                // throw new notfoundexception();
            }
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
