﻿using Mefit_BE.Models.Entities;

namespace Mefit_BE.Services.ProgramCategoryServices
{
    public interface IProgramCategoryService : ICrudService<ProgramCategory, int>
    {
        public Task<bool> programCategoryExists(int id);
    }
}
