﻿using Mefit_BE.Models.Entities;

namespace Mefit_BE.Services.FitnessPrograms
{
    public interface IFitnessProgramService : ICrudService<FitnessProgram, int>
    {
        public Task<bool> FitnessProgramExistsAsync(int id);
        public Task UpdateWorkoutsAsync(int[] WorkoutId, int id);
    }
}
