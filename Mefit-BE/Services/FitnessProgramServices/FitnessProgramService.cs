﻿using Mefit_BE.Models;
using Mefit_BE.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Mefit_BE.Services.FitnessPrograms
{
    public class FitnessProgramService : IFitnessProgramService
    {
        private readonly MeFitDbContext _context;
        private readonly ILogger<FitnessProgramService> _logger;

        public FitnessProgramService(MeFitDbContext context, ILogger<FitnessProgramService> logger)
        {
            _context = context;
            _logger = logger;
        }



        public async Task<FitnessProgram> AddAsync(FitnessProgram entity)
        {
            await _context.Programs.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            if (!await FitnessProgramExistsAsync(id))
            {
                _logger.LogError("Program Category not found with Id: " + id);
                // throw new notfoundexception();
            }
            var fitnessProgram = await _context.Programs.FindAsync(id);
            _context.Programs.Remove(fitnessProgram);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> FitnessProgramExistsAsync(int id)
        {
            return await _context.Programs.AnyAsync(p => p.Program_id == id);
        }

        public async Task<ICollection<FitnessProgram>> GetAllAsync()
        {
            return await _context.Programs
               .Include(p => p.programCategory)
               .Include(p => p.Workouts)
               .ToListAsync();
        }

        public async Task<FitnessProgram> GetByIdAsync(int id)
        {
            if (!await FitnessProgramExistsAsync(id))
            {
                _logger.LogError("Fitness program not found with Id: " + id);
                // throw new notfoundexception();
            }

            return await _context.Programs
                .Where(p => p.Program_id == id)
                .Include(p => p.Workouts)
                .FirstAsync();
        }

        public async Task UpdateAsync(FitnessProgram entity)
        {
            if (!await FitnessProgramExistsAsync(entity.Program_id))
            {
                _logger.LogError("Fitness Program not found with Id: " + entity.Program_id);
                // throw new notfoundexception();
            }
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateWorkoutsAsync(int[] WorkoutId, int id)
        {
            if (!await FitnessProgramExistsAsync(id))
            {
                _logger.LogError("Fitness Program not found with Id: " + id);
            }

            List<Workout> workouts = WorkoutId.ToList()
                .Select(wId => _context.Workouts
                .Where(w => w.Workout_id == wId).First()).ToList();

             FitnessProgram fitnessProgram = await _context.Programs
                .Where(p => p.Program_id == id)
                .FirstAsync();
            fitnessProgram.Workouts = workouts;
            _context.Entry(fitnessProgram).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
