﻿using Mefit_BE.Models.Entities;

namespace Mefit_BE.Services.Sets
{
    public interface ISetService : ICrudService<Set, int>
    {
        /// <summary> 
        /// Check if the set exists by id.
        /// <param name="id">
        /// </summary>
        public Task<bool> SetExistsAsync(int id);
    }
}
