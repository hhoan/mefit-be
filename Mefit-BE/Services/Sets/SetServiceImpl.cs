﻿using Mefit_BE.Models.Entities;
using Mefit_BE.Models;
using Mefit_BE.Services.Sets;
using Microsoft.EntityFrameworkCore;

namespace Mefit_BE.Services.Sets
{
    public class SetServiceImpl : ISetService
    {
        /// <summary>
        /// Class to house business logic
        /// Interacts with entity framework
        /// Service will be injectable and used in Controller
        /// Context refers to database
        /// </summary>
        /// 

        private readonly MeFitDbContext _context;
        private readonly ILogger<SetServiceImpl> _logger;

        public SetServiceImpl(MeFitDbContext postgradEf, ILogger<SetServiceImpl> logger)
        {
            _context = postgradEf;
            _logger = logger;
        }

        // get all
        public async Task<ICollection<Set>> GetAllAsync()
        {
            return await _context.Sets
                .Include(s => s.Exercise)
                .Include(s => s.Workouts)
                .ToListAsync();
        }

        // GET by id
        public async Task<Set> GetByIdAsync(int id)
        {
            if (!await SetExistsAsync(id))
            {
                _logger.LogError("Set not found with Id: " + id);
                // throw new notfoundexception();
            }

            return await _context.Sets
                .Where(s => s.Set_id == id)
                .Include(s => s.Exercise)
                .Include(s => s.Workouts)
                .FirstAsync();
        }

        // post
        public async Task<Set> AddAsync(Set entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        // update
        public async Task UpdateAsync(Set entity)
        {
            if (!await SetExistsAsync(entity.Set_id))
            {
                _logger.LogError("Set not found with Id: " + entity.Set_id);
            }
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        // delete 
        public async Task DeleteAsync(int id)
        {
            var set = await _context.Sets.FindAsync(id);

            if (set == null)
            {
                _logger.LogError("Set not found with Id: " + id);
            }

            _context.Sets.Remove(set);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> SetExistsAsync(int id)
        {
            return await _context.Sets.AnyAsync(s => s.Set_id == id);
        }
    }
}
