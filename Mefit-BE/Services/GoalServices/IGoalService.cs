﻿using Mefit_BE.Models.Entities;

namespace Mefit_BE.Services.GoalServices
{
    public interface IGoalService :ICrudService<Goal, int>
    {
        public Task<bool> GoalExistsAsync(int id);
        public Task<Goal?> GetLastGoalOfCurrentUser(string userId);

        public Task UpdateWorkoutsAsync(int[] WorkoutId, int id);
    }
}
