﻿using Mefit_BE.Models;
using Mefit_BE.Models.Entities;
using Mefit_BE.Services.MuscleGroups;
using Microsoft.EntityFrameworkCore;

namespace Mefit_BE.Services.GoalServices
{
    public class GoalService : IGoalService
    {

        private readonly MeFitDbContext _context;
        private readonly ILogger<GoalService> _logger;

        public GoalService(MeFitDbContext context, ILogger<GoalService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Goal> AddAsync(Goal entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            if (!await GoalExistsAsync(id))
            {
                _logger.LogError("Goal not found with Id: " + id);
                // throw new notfoundexception();
            }
            var goal = await _context.Goals.FindAsync(id);
            _context.Goals.Remove(goal);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Goal>> GetAllAsync()
        {
            return await _context.Goals
               .Include(g => g.Program)
               .Include(g => g.Workouts)
               .ToListAsync();
        }

        public async Task<Goal> GetByIdAsync(int id)
        {
            if (!await GoalExistsAsync(id))
            {
                _logger.LogError("Goal not found with Id: " + id);
            }

            return await _context.Goals
                .Where(g => g.Goal_id == id)
                .Include(g => g.Workouts)
                .Include(g => g.Program)
                .FirstAsync();
        }

        public async Task<Goal?> GetLastGoalOfCurrentUser(string userId)
        {
            var lastGoalOfCurrentUser = await _context.Goals
                .Where(g => g.UserProfile_id == userId)
                .Include(g => g.Program)
                .ThenInclude(p => p.Workouts)
                .OrderBy(g => g.Goal_id)
                .LastAsync();
                
            return lastGoalOfCurrentUser;
        }

        public async Task UpdateAsync(Goal entity)
        {
            if (!await GoalExistsAsync(entity.Goal_id))
            {
                _logger.LogError("Goal not found with Id: " + entity.Goal_id);
                // throw new notfoundexception();
            }
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<bool> GoalExistsAsync(int id)
        {
            return await _context.Goals.AnyAsync(g => g.Goal_id == id);
        }


        public async Task UpdateWorkoutsAsync(int[] WorkoutId, int id)
        {
            if (!await GoalExistsAsync(id))
            {
                _logger.LogError("Goal not found with Id: " + id);
            }

            List<Workout> workouts = WorkoutId.ToList()
                .Select(wId => _context.Workouts
                .Where(w => w.Workout_id == wId).First()).ToList();

            Goal goal = await _context.Goals
               .Where(g => g.Goal_id == id)
               .FirstAsync();
            goal.Workouts = workouts;
            _context.Entry(goal).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
