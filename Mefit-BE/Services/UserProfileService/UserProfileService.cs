﻿using Mefit_BE.Models;
using Mefit_BE.Models.Entities;
using Mefit_BE.Utils;
using Microsoft.EntityFrameworkCore;

namespace Mefit_BE.Services.UserProfileService
{
    public class UserProfileService : IUserProfileService
    {
        private readonly MeFitDbContext _context;

        public UserProfileService(MeFitDbContext context)
        {
            _context = context;
        }
        public async Task<UserProfile> AddAsync(UserProfile entity)
        {
            await _context.UserProfiles.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(string id)
        {
            var userProfile = await _context.UserProfiles.FindAsync(id);
            _context.UserProfiles.Remove(userProfile);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<UserProfile>> GetAllAsync()
        {
            return await _context.UserProfiles
                .Include(p => p.Goals)
                .ThenInclude(g => g.Program)
                .ThenInclude(p => p.Workouts)
                .ToListAsync();
        }

        public async Task<UserProfile> GetByIdAsync(string id)
        {
            return await _context.UserProfiles
                .Where(p => p.Id == id)
                .Include(p => p.Goals)
                .ThenInclude(g => g.Program)
                .ThenInclude(p => p.Workouts)
                .FirstAsync();
        }

        public async Task UpdateAsync(UserProfile entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<bool> UserProfileExists(string id)
        {
            return await _context.UserProfiles.AnyAsync(p => p.Id == id);
        }

        public async Task UpdateGoalsAsync(int[] GoalId, string id)
        {

            List<Goal> goals = GoalId.ToList()
                .Select(gId => _context.Goals
                .Where(g => g.Goal_id == gId).First()).ToList();

            UserProfile profile = await _context.UserProfiles
               .Where(p => p.Id == id)
               .FirstAsync();
            profile.Goals = goals;
            _context.Entry(profile).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Exercise>> GetExercisesAsync(string id)
        {
            if (!await UserProfileExists(id))
            {
                throw new EntityNotFoundException();
            }
            UserProfile userprofile = await _context.UserProfiles
                .Where(p => p.Id == id)
                .Include(p => p.Goals)
                .ThenInclude(g => g.Program)
                .ThenInclude(p => p.Workouts)
                .ThenInclude(w => w.Sets)
                .ThenInclude(s => s.Exercise)
                .FirstAsync();
            ICollection<Exercise> exercises = new List<Exercise>();
            foreach (var goal in userprofile.Goals)
            {
                foreach (var workout in goal.Program.Workouts)
                {
                    foreach (var set in workout.Sets)
                    {
                        if (!exercises.Contains(set.Exercise))
                        {
                            exercises.Add(set.Exercise);
                        }
                    }
                }
            }
            return exercises;


        }

        public async Task<ICollection<Workout>> GetWorkoutsAsync(string id)
        {
            if (!await UserProfileExists(id))
            {
                throw new EntityNotFoundException();
            }
            UserProfile userprofile = await _context.UserProfiles
                .Where(p => p.Id == id)
                .Include(p => p.Goals)
                .ThenInclude(g => g.Program)
                .ThenInclude(p => p.Workouts)
                .FirstAsync();
            ICollection<Workout> workouts = new List<Workout>();
            foreach (var goal in userprofile.Goals)
            {
                foreach (var workout in goal.Program.Workouts)
                {
                    if (!workouts.Contains(workout))
                    {
                        workouts.Add(workout);
                    }
                }
            }
            return workouts;
        }

    }
}
