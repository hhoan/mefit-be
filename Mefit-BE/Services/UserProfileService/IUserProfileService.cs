﻿using Mefit_BE.Models.Entities;

namespace Mefit_BE.Services.UserProfileService
{
    public interface IUserProfileService : ICrudService<UserProfile, string>
    {
        public Task<bool> UserProfileExists(string id);
        public Task UpdateGoalsAsync(int[] GoalId, string id);
        Task<ICollection<Exercise>> GetExercisesAsync(string id); //include sets
        Task<ICollection<Workout>> GetWorkoutsAsync(string id);

    }
}
