﻿//using Mefit_BE.Models.DTOs.Exercises;
using Mefit_BE.Models.Entities;


namespace Mefit_BE.Services.MuscleGroups
{
    public interface IMuscleGroupService : ICrudService<MuscleGroup,int>
    {
        /// <summary> 
        /// Get exercises for a muscle group by muscle group ID.
        /// <param name="muscleGroupId">
        /// </summary>
        public Task<ICollection<Exercise>> GetExercisesAsync(int muscleGroupId);

        /// <summary> 
        /// Check if the muscle group exists by id.
        /// <param name="id">
        /// </summary>
        public Task<bool> MuscleGroupExistsAsync(int id);
    }
}