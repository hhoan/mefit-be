﻿using AutoMapper;
using Mefit_BE.Models.DTOs.Exercises;
using Mefit_BE.Models;
using Mefit_BE.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Mefit_BE.Services.MuscleGroups
{
    public class MuscleGroupServiceImpl : IMuscleGroupService
    {
        /// <summary>
        /// Class to house business logic
        /// Interacts with entity framework
        /// Service will be injectable and used in Controller
        /// Context refers to database
        /// </summary>
        /// 

        private readonly MeFitDbContext _context;
        private readonly ILogger<MuscleGroupServiceImpl> _logger;

        public MuscleGroupServiceImpl(MeFitDbContext postgradEf, ILogger<MuscleGroupServiceImpl> logger)
        {
            _context = postgradEf;
            _logger = logger;
        }

        // get all
        public async Task<ICollection<MuscleGroup>> GetAllAsync()
        {
            return await _context.MuscleGroups
                .Include(m => m.Exercises)
                .ToListAsync();
        }

        // GET by id
        public async Task<MuscleGroup> GetByIdAsync(int id)
        {
            if (!await MuscleGroupExistsAsync(id))
            {
                _logger.LogError("Muscle group not found with Id: " + id);
                // throw new notfoundexception();
            }

            return await _context.MuscleGroups
                .Where(m => m.Id == id)
                .Include(m => m.Exercises)
                .FirstAsync();
        }

        // Get exercises for a musclegroup
        public async Task<ICollection<Exercise>> GetExercisesAsync(int muscleGroupId) 
        {
            if (!await MuscleGroupExistsAsync(muscleGroupId))
            {
                _logger.LogError("MuscleGroup not found with Id: " + muscleGroupId);
                // throw new notfoundexception();
            }

            return await _context.Exercises
                .Where(e =>  e.MuscleGroup_Id == muscleGroupId)
                .ToListAsync();
        }

            // post
            public async Task<MuscleGroup> AddAsync(MuscleGroup entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        // update
        public async Task UpdateAsync(MuscleGroup entity)
        {
            if (!await MuscleGroupExistsAsync(entity.Id))
            {
                _logger.LogError("Musclegroup not found with Id: " + entity.Id);
            }
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

            // delete 
            public async Task DeleteAsync(int id)
            {
            var muscleGroup = await _context.MuscleGroups.FindAsync(id);    

            if (muscleGroup == null)
            {
                _logger.LogError("Musclegroup not found with Id: " + id);
            }

            _context.MuscleGroups.Remove(muscleGroup);
            await _context.SaveChangesAsync();
            }

        public async Task<bool> MuscleGroupExistsAsync(int id)
        {
            return await _context.MuscleGroups.AnyAsync(m => m.Id == id);
        }
    }
}

