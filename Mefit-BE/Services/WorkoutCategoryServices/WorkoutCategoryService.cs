﻿using Mefit_BE.Models;
using Mefit_BE.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Mefit_BE.Services.WorkoutCategoryServices
{
    public class WorkoutCategoryService : IWorkoutCategoryService
    {
        private readonly MeFitDbContext _context;
        private readonly ILogger<WorkoutCategoryService> _logger;
        public WorkoutCategoryService(MeFitDbContext context, ILogger<WorkoutCategoryService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<WorkoutCategory> AddAsync(WorkoutCategory entity)
        {
            await _context.WorkoutCategories.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            if (!await workoutCategoryExists(id))
            {
                _logger.LogError("Workout Category not found with Id: " + id);
                // throw new notfoundexception();
            }
            var workoutCategory = await _context.WorkoutCategories.FindAsync(id);
            _context.WorkoutCategories.Remove(workoutCategory);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<WorkoutCategory>> GetAllAsync()
        {
            return await _context.WorkoutCategories.ToListAsync();
        }

        public async Task<WorkoutCategory> GetByIdAsync(int id)
        {
            if (!await workoutCategoryExists(id))
            {
                _logger.LogError("Workout Category not found with Id: " + id);
                // throw new notfoundexception();
            }
            return await _context.WorkoutCategories.FindAsync(id);
        }

        public async Task UpdateAsync(WorkoutCategory entity)
        {
            if (!await workoutCategoryExists(entity.Id))
            {
                _logger.LogError("Workout Category not found with Id: " + entity.Id);
                // throw new notfoundexception();
            }
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }

        public async Task<bool> workoutCategoryExists(int id)
        {
            return await _context.WorkoutCategories.AnyAsync(w => w.Id == id);
        }
    }
}
