﻿using Mefit_BE.Models.Entities;

namespace Mefit_BE.Services.WorkoutCategoryServices
{
    public interface IWorkoutCategoryService : ICrudService<WorkoutCategory, int>
    {
        public Task<bool> workoutCategoryExists(int id);
    }
}
