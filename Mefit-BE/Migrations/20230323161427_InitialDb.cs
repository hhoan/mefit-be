﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Mefit_BE.Migrations
{
    /// <inheritdoc />
    public partial class InitialDb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MuscleGroup",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MuscleGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProgramCatergory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramCatergory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserProfile",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    Weight = table.Column<double>(type: "float", nullable: true),
                    Height = table.Column<double>(type: "float", nullable: true),
                    FitnessLevel = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FitnessGoal = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProfile", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkoutCategory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkoutCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Excercise",
                columns: table => new
                {
                    Exercise_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1028)", maxLength: 1028, nullable: false),
                    MuscleGroup_Id = table.Column<int>(type: "int", nullable: false),
                    Image = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: true),
                    VidLink = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Excercise", x => x.Exercise_id);
                    table.ForeignKey(
                        name: "FK_Excercise_MuscleGroup_MuscleGroup_Id",
                        column: x => x.MuscleGroup_Id,
                        principalTable: "MuscleGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Program",
                columns: table => new
                {
                    Program_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Category_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Program", x => x.Program_id);
                    table.ForeignKey(
                        name: "FK_Program_ProgramCatergory_Category_id",
                        column: x => x.Category_id,
                        principalTable: "ProgramCatergory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Workout",
                columns: table => new
                {
                    Workout_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WorkoutName = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    Complete = table.Column<bool>(type: "bit", nullable: false),
                    WorkoutCategory_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workout", x => x.Workout_id);
                    table.ForeignKey(
                        name: "FK_Workout_WorkoutCategory_WorkoutCategory_id",
                        column: x => x.WorkoutCategory_id,
                        principalTable: "WorkoutCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Set",
                columns: table => new
                {
                    Set_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Exercise_repetitions = table.Column<int>(type: "int", nullable: false),
                    Set_amount = table.Column<int>(type: "int", nullable: false),
                    Exercise_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Set", x => x.Set_id);
                    table.ForeignKey(
                        name: "FK_Set_Excercise_Exercise_id",
                        column: x => x.Exercise_id,
                        principalTable: "Excercise",
                        principalColumn: "Exercise_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Goal",
                columns: table => new
                {
                    Goal_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Achieved = table.Column<bool>(type: "bit", nullable: false),
                    GoalProgram_Id = table.Column<int>(type: "int", nullable: true),
                    UserProfile_id = table.Column<string>(type: "nvarchar(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Goal", x => x.Goal_id);
                    table.ForeignKey(
                        name: "FK_Goal_Program_GoalProgram_Id",
                        column: x => x.GoalProgram_Id,
                        principalTable: "Program",
                        principalColumn: "Program_id");
                    table.ForeignKey(
                        name: "FK_Goal_UserProfile_UserProfile_id",
                        column: x => x.UserProfile_id,
                        principalTable: "UserProfile",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkoutProgram",
                columns: table => new
                {
                    Program_Id = table.Column<int>(type: "int", nullable: false),
                    Workout_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkoutProgram", x => new { x.Program_Id, x.Workout_Id });
                    table.ForeignKey(
                        name: "FK_WorkoutProgram_Program_Program_Id",
                        column: x => x.Program_Id,
                        principalTable: "Program",
                        principalColumn: "Program_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WorkoutProgram_Workout_Workout_Id",
                        column: x => x.Workout_Id,
                        principalTable: "Workout",
                        principalColumn: "Workout_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SetWorkout",
                columns: table => new
                {
                    Set_Id = table.Column<int>(type: "int", nullable: false),
                    Workout_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SetWorkout", x => new { x.Set_Id, x.Workout_Id });
                    table.ForeignKey(
                        name: "FK_SetWorkout_Set_Set_Id",
                        column: x => x.Set_Id,
                        principalTable: "Set",
                        principalColumn: "Set_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SetWorkout_Workout_Workout_Id",
                        column: x => x.Workout_Id,
                        principalTable: "Workout",
                        principalColumn: "Workout_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GoalWorkout",
                columns: table => new
                {
                    Goal_Id = table.Column<int>(type: "int", nullable: false),
                    Workout_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoalWorkout", x => new { x.Goal_Id, x.Workout_Id });
                    table.ForeignKey(
                        name: "FK_GoalWorkout_Goal_Goal_Id",
                        column: x => x.Goal_Id,
                        principalTable: "Goal",
                        principalColumn: "Goal_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GoalWorkout_Workout_Workout_Id",
                        column: x => x.Workout_Id,
                        principalTable: "Workout",
                        principalColumn: "Workout_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "MuscleGroup",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Chest" },
                    { 2, "Legs" },
                    { 3, "Back" },
                    { 4, "Arms" },
                    { 5, "Core" },
                    { 6, "Shoulder" }
                });

            migrationBuilder.InsertData(
                table: "ProgramCatergory",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Strength" },
                    { 2, "Cardiovascular" },
                    { 3, "Endurance" },
                    { 4, "Strength and Cardio" }
                });

            migrationBuilder.InsertData(
                table: "UserProfile",
                columns: new[] { "Id", "FitnessGoal", "FitnessLevel", "Height", "Weight" },
                values: new object[,]
                {
                    { "3846e120-6814-4140-9bd4-c6b53d989582", "Maintain weight", "Advanced", 180.0, 80.099999999999994 },
                    { "3bb30c7b-2b03-4efa-a108-de0fb92b92bd", "Weight loss", "Beginner", 165.0, 60.200000000000003 },
                    { "d7a65416-a8eb-40ac-beeb-f32e91e3de9d", "Muscle gain", "Intermediate", 170.0, 70.5 }
                });

            migrationBuilder.InsertData(
                table: "WorkoutCategory",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Strength Training" },
                    { 2, "Cardiovascular Exercise" },
                    { 3, "Yoga" }
                });

            migrationBuilder.InsertData(
                table: "Excercise",
                columns: new[] { "Exercise_id", "Description", "Image", "MuscleGroup_Id", "Name", "VidLink" },
                values: new object[,]
                {
                    { 1, "Lie on your back on a bench, grip a barbell with both hands and lower it to your chest, then push it back up to straight arms. This exercise primarily targets the chest muscles and also works the shoulders and triceps.", null, 1, "Bench Press", null },
                    { 2, "Stand with your feet shoulder-width apart, hold a barbell on your shoulders, and lower yourself until your thighs are parallel to the ground, then push back up to standing. This exercise targets the lower body muscles, including the quadriceps, hamstrings, and glutes.", null, 2, "Squats", null },
                    { 3, "Stand with your feet shoulder-width apart, grip a barbell with both hands, then lift it off the ground by extending your hips and knees until you're standing straight up. This exercise primarily targets the back muscles and also works the legs and grip strength.", null, 3, "Deadlifts", null },
                    { 4, "Hold a dumbbell in each hand with your palms facing up, then bend your arms to bring the weights towards your shoulders, and lower them back down. This exercise targets the biceps.", null, 4, "Bicep curl", null },
                    { 5, "Hold a dumbbell in each hand with your palms facing down, then extend your arms straight back behind you, and lower them back down. This exercise targets the triceps.", null, 4, "Tricep curl", null },
                    { 6, "Get into a pushup position but instead of lowering down, hold yourself in a straight line from head to heels, with your elbows and forearms on the ground. This exercise targets the core muscles, including the abdominals and lower back.", null, 5, "Plank", null },
                    { 7, "Get into a plank position with your hands on the ground, then lower your body down to the ground and push back up to straight arms. This exercise targets the chest, shoulders, and triceps.", null, 4, "Pushup", null },
                    { 8, "Grip a bar overhead with your hands facing away from you, then pull your body up until your chin is above the bar, then lower back down. This exercise targets the back muscles and also works the arms and shoulders.", null, 4, "Pullup", null },
                    { 9, "Stair running, involves running up and down stairs as a form of cardiovascular exercise that targets the lower body, particularly the quadriceps, hamstrings, and glutes. This exercise can be performed outdoors or indoors, and can be modified to increase or decrease intensity by adjusting the speed and number of stairs climbed.", null, 2, "Stair running", null },
                    { 10, "Downward dog, is a yoga pose that involves starting on all fours, then lifting the hips and straightening the legs to form an inverted V-shape with the body. This pose targets the hamstrings, calves, and shoulders, and can help to improve flexibility, balance, and posture.", null, 5, "Downward dog", null },
                    { 11, "Shoulder press, involves lifting weights overhead while seated or standing to target the deltoids, triceps, and upper back muscles. This exercise can be performed with dumbbells, barbells, or resistance bands, and can help to improve upper body strength and posture.", null, 6, "Shoulder press", null },
                    { 12, "Upward dog, is a yoga pose that involves starting on the stomach, then lifting the chest and upper body while keeping the legs and lower body on the ground. This pose targets the chest, shoulders, and back, and can help to improve flexibility and posture.", null, 1, "Upward dog", null },
                    { 13, "Warrior one, is a yoga pose that involves starting in a lunge position with the back foot turned out, then lifting the arms overhead and squaring the hips to the front. This pose targets the quadriceps, hamstrings, and glutes, and can help to improve balance and lower body strength.", null, 2, "Warrior one", null },
                    { 14, "Crow pose is a yoga pose. To perform the Crow pose, start in a squat position and place your hands on the ground in front of you. Then, lift your feet off the ground and balance on your hands while keeping your knees on your arms. This yoga pose primarily targets the arm and shoulder muscles, such as the triceps, biceps, and deltoids, as well as the wrists and core muscles.", null, 4, "Crow pose", null },
                    { 15, "To perform Squat jumping, start in a squat position and then jump as high as you can, using your leg muscles to power the movement. This plyometric exercise primarily targets the leg muscles, including the quadriceps, hamstrings, and calves, as well as the glutes and core muscles for stability and power.", null, 2, "Squat jumping", null },
                    { 16, "To perform interval running, alternate periods of high-intensity running with periods of lower-intensity recovery. This type of cardio workout targets the leg muscles, including the quadriceps, hamstrings, calves, and glutes, as well as the core muscles, and can improve cardiovascular endurance.\r\n", null, 2, "Interval running", null },
                    { 17, "To perform leg curls, lie face down on a machine with your legs hooked under a padded bar, then curl your legs up towards your buttocks by contracting your hamstrings. This exercise specifically targets the hamstring muscles, which are the muscles on the back of your thighs.", null, 2, "Leg curl", null }
                });

            migrationBuilder.InsertData(
                table: "Program",
                columns: new[] { "Program_id", "Category_id", "Name" },
                values: new object[,]
                {
                    { 1, 2, "Explosive Training Program" },
                    { 2, 1, "Strength Program" },
                    { 3, 4, "HIIT Program" },
                    { 4, 3, "Endurance Program" }
                });

            migrationBuilder.InsertData(
                table: "Workout",
                columns: new[] { "Workout_id", "Complete", "WorkoutCategory_id", "WorkoutName" },
                values: new object[,]
                {
                    { 1, false, 1, "Upper Body Workout" },
                    { 2, false, 1, "Lower Body Workout" },
                    { 3, false, 3, "Explosive Full Body Workout" },
                    { 4, false, 1, "Explosive Upper Body Workout" },
                    { 5, false, 1, "Core Workout" },
                    { 6, false, 3, "Endurance Core Workout" },
                    { 7, false, 1, "Cardivascular Health Workout" },
                    { 8, false, 1, "Vinyasa Yoga Workout" },
                    { 9, false, 3, "Endurance Cardivascular Workout" }
                });

            migrationBuilder.InsertData(
                table: "Goal",
                columns: new[] { "Goal_id", "Achieved", "EndDate", "GoalProgram_Id", "UserProfile_id" },
                values: new object[,]
                {
                    { 1, false, new DateTime(2023, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "d7a65416-a8eb-40ac-beeb-f32e91e3de9d" },
                    { 2, false, new DateTime(2024, 6, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "3bb30c7b-2b03-4efa-a108-de0fb92b92bd" },
                    { 3, true, new DateTime(2023, 9, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "3846e120-6814-4140-9bd4-c6b53d989582" }
                });

            migrationBuilder.InsertData(
                table: "Set",
                columns: new[] { "Set_id", "Exercise_id", "Exercise_repetitions", "Set_amount" },
                values: new object[,]
                {
                    { 1, 1, 10, 3 },
                    { 2, 2, 8, 4 },
                    { 3, 3, 12, 2 },
                    { 4, 4, 10, 3 },
                    { 5, 5, 8, 4 },
                    { 6, 6, 12, 2 },
                    { 7, 7, 10, 3 },
                    { 8, 8, 8, 4 },
                    { 9, 9, 15, 10 },
                    { 10, 10, 1, 5 },
                    { 11, 11, 12, 2 },
                    { 12, 12, 1, 5 },
                    { 13, 13, 1, 5 },
                    { 14, 14, 1, 5 },
                    { 15, 15, 10, 3 },
                    { 16, 16, 15, 5 },
                    { 17, 17, 12, 3 }
                });

            migrationBuilder.InsertData(
                table: "WorkoutProgram",
                columns: new[] { "Program_Id", "Workout_Id" },
                values: new object[,]
                {
                    { 1, 3 },
                    { 1, 4 },
                    { 1, 7 },
                    { 2, 1 },
                    { 2, 2 },
                    { 2, 5 },
                    { 3, 3 },
                    { 3, 6 },
                    { 3, 7 },
                    { 4, 6 },
                    { 4, 8 },
                    { 4, 9 }
                });

            migrationBuilder.InsertData(
                table: "SetWorkout",
                columns: new[] { "Set_Id", "Workout_Id" },
                values: new object[,]
                {
                    { 1, 3 },
                    { 1, 4 },
                    { 2, 2 },
                    { 2, 3 },
                    { 2, 5 },
                    { 2, 9 },
                    { 3, 3 },
                    { 3, 5 },
                    { 3, 6 },
                    { 3, 7 },
                    { 4, 1 },
                    { 5, 1 },
                    { 6, 5 },
                    { 6, 6 },
                    { 7, 1 },
                    { 7, 4 },
                    { 7, 6 },
                    { 7, 9 },
                    { 8, 1 },
                    { 8, 4 },
                    { 9, 2 },
                    { 9, 3 },
                    { 9, 7 },
                    { 9, 9 },
                    { 10, 8 },
                    { 11, 4 },
                    { 12, 8 },
                    { 13, 8 },
                    { 14, 5 },
                    { 14, 8 },
                    { 15, 2 },
                    { 15, 7 },
                    { 16, 6 },
                    { 16, 7 },
                    { 16, 9 },
                    { 17, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Excercise_MuscleGroup_Id",
                table: "Excercise",
                column: "MuscleGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Goal_GoalProgram_Id",
                table: "Goal",
                column: "GoalProgram_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Goal_UserProfile_id",
                table: "Goal",
                column: "UserProfile_id");

            migrationBuilder.CreateIndex(
                name: "IX_GoalWorkout_Workout_Id",
                table: "GoalWorkout",
                column: "Workout_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Program_Category_id",
                table: "Program",
                column: "Category_id");

            migrationBuilder.CreateIndex(
                name: "IX_Set_Exercise_id",
                table: "Set",
                column: "Exercise_id");

            migrationBuilder.CreateIndex(
                name: "IX_SetWorkout_Workout_Id",
                table: "SetWorkout",
                column: "Workout_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Workout_WorkoutCategory_id",
                table: "Workout",
                column: "WorkoutCategory_id");

            migrationBuilder.CreateIndex(
                name: "IX_WorkoutProgram_Workout_Id",
                table: "WorkoutProgram",
                column: "Workout_Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GoalWorkout");

            migrationBuilder.DropTable(
                name: "SetWorkout");

            migrationBuilder.DropTable(
                name: "WorkoutProgram");

            migrationBuilder.DropTable(
                name: "Goal");

            migrationBuilder.DropTable(
                name: "Set");

            migrationBuilder.DropTable(
                name: "Workout");

            migrationBuilder.DropTable(
                name: "Program");

            migrationBuilder.DropTable(
                name: "UserProfile");

            migrationBuilder.DropTable(
                name: "Excercise");

            migrationBuilder.DropTable(
                name: "WorkoutCategory");

            migrationBuilder.DropTable(
                name: "ProgramCatergory");

            migrationBuilder.DropTable(
                name: "MuscleGroup");
        }
    }
}
