﻿using Microsoft.AspNetCore.Mvc;
using Mefit_BE.Models.Entities;
using Mefit_BE.Services.FitnessPrograms;
using AutoMapper;
using Mefit_BE.Models.DTOs.FitnessPrograms;
using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;
using Mefit_BE.Utils;

namespace Mefit_BE.Controllers
{
    [Route("api/v1/fitness-programs")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class FitnessProgramController : ControllerBase
    {
        private readonly IFitnessProgramService _service;
        private readonly IMapper _mapper;

        public FitnessProgramController(IFitnessProgramService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: api/v1/FitnessProgram
        /// <summary>
        /// Get all Fitness Programs from DB
        /// </summary>
        /// <returns>List of fitness programs</returns>
        [HttpGet]
        public async Task<ActionResult<ICollection<FitnessProgramDTO>>> GetPrograms()
        {
            return Ok(_mapper.Map<ICollection<FitnessProgramDTO>>(await _service.GetAllAsync()));
        }

        // GET: api/v1/FitnessProgram/{id}
        /// <summary>
        /// Get Fitness Program by ID from DB
        /// </summary>
        /// <param name="id"></param>
        /// <returns>FitnessProgram object</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FitnessProgramDTO>> GetFitnessProgram(int id)
        {
            try
            {
                return Ok(_mapper.Map<FitnessProgramDTO>(await _service.GetByIdAsync(id)));
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        // PUT: api/v1/FitnessProgram/{id}
        /// <summary>
        /// Update FitnessProgram by ID in DB
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fitnessProgram"></param>
        /// <returns>Action result</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFitnessProgram(int id, FitnessProgramPutDTO fitnessProgram)
        {
            if (id != fitnessProgram.Program_id)
            {
                return BadRequest();
            }
            if (!await _service.FitnessProgramExistsAsync(id))
            {
                return NotFound();
            }

            await _service.UpdateAsync(_mapper.Map<FitnessProgram>(fitnessProgram));
            return NoContent();
        }

        // POST: api/v1/FitnessProgram
        /// <summary>
        /// Add new FitnessProgram to DB
        /// </summary>
        /// <param name="fitnessProgramDTO"></param>
        /// <returns>Action result</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<FitnessProgramPostDTO>> PostFitnessProgram(FitnessProgramPostDTO fitnessProgramDTO)
        {
            FitnessProgram fitnessProgram = _mapper.Map<FitnessProgram>(fitnessProgramDTO);
            fitnessProgram = await _service.AddAsync(fitnessProgram);
            return CreatedAtAction("GetFitnessProgram", new { id = fitnessProgram.Program_id }, fitnessProgram);
        }

        // DELETE: api/v1/FitnessProgram/{id}
        /// <summary>
        /// Delete FitnessProgram by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFitnessProgram(int id)
        {
            if (!await _service.FitnessProgramExistsAsync(id))
            {
                return NotFound();
            }
            await _service.DeleteAsync(id);
            return NoContent();
        }


        // PUT: api/v1/{id}/workouts
        /// <summary>
        /// Update Workouts in a Program by ID in DB
        /// </summary>
        /// <param name="WorkoutId"></param>
        /// <param name="id">Workout Id</param>
        /// <returns>Action result</returns>
        [HttpPut("{id}/workouts")]
        public async Task<IActionResult> UpdateWorkoutsInProgramAsync(int[] WorkoutId, int id)
        {
            await _service.UpdateWorkoutsAsync(WorkoutId, id);
            return NoContent();
        }
    }
}
