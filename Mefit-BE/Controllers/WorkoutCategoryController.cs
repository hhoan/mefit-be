﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mefit_BE.Models;
using Mefit_BE.Models.Entities;
using Mefit_BE.Services.WorkoutCategoryServices;
using System.Net.Mime;
using AutoMapper;
using Mefit_BE.Models.DTOs.WorkoutCategories;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Mefit_BE.Models.DTOs.ProgramCategories;
using Mefit_BE.Utils;

namespace Mefit_BE.Controllers
{
    [Route("api/v1/workout-categories")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class WorkoutCategoryController : ControllerBase
    {
        private readonly IWorkoutCategoryService _service;
        private readonly IMapper _mapper;

        public WorkoutCategoryController(IWorkoutCategoryService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: api/v1/WorkoutCategory
        /// <summary>
        /// Get all workout categories in the DB.
        /// </summary>
        /// <returns>List of Workout Categories</returns>
        [HttpGet]
        public async Task<ActionResult<ICollection<WorkoutCategoryDTO>>> GetWorkoutCategories()
        {
            return Ok(_mapper.Map<ICollection<WorkoutCategoryDTO>>(await _service.GetAllAsync()));
        }

        // GET: api/v1/WorkoutCategory/{id}
        /// <summary>
        /// Get workout category by ID from DB.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Workout category object</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<WorkoutCategoryDTO>> GetWorkoutCategory(int id)
        {
            try
            {
                return Ok(_mapper.Map<WorkoutCategoryDTO>(await _service.GetByIdAsync(id)));
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }

        }

        // PUT: api/v1/WorkoutCategory/{id}
        /// <summary>
        /// Update workout category by ID from DB.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="workoutCategory"></param>
        /// <returns>Action result</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWorkoutCategory(int id, WorkoutCategoryPutDTO workoutCategory)
        {
            if (id != workoutCategory.Id)
            {
                return BadRequest();
            }
            if (! await _service.workoutCategoryExists(id))
            {
                return NotFound();
            }

            await _service.UpdateAsync(_mapper.Map<WorkoutCategory>(workoutCategory));
            return NoContent();

        }

        // POST: api/v1/WorkoutCategory
        /// <summary>
        /// Add new workout category to DB.
        /// </summary>
        /// <param name="workoutCategory"></param>
        /// <returns>Action result</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<WorkoutCategoryPostDTO>> PostWorkoutCategory(WorkoutCategoryPostDTO workoutCategoryDTO)
        {
            WorkoutCategory workoutCategory = _mapper.Map<WorkoutCategory>(workoutCategoryDTO);
            workoutCategory = await _service.AddAsync(workoutCategory);
            return CreatedAtAction("GetWorkoutCategory", new {id = workoutCategory.Id}, workoutCategory);
        }

        // DELETE: api/v1/WorkoutCategory/{id}
        /// <summary>
        /// Delete Workout Category by ID from DB.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Action Result</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWorkoutCategory(int id)
        {
            if (! await _service.workoutCategoryExists(id))
            {
                return NotFound();
            }
            await _service.DeleteAsync(id);
            return NoContent();

        }
    }
}
