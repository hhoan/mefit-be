﻿using Microsoft.AspNetCore.Mvc;
using Mefit_BE.Models.Entities;
using AutoMapper;
using System.Net.Mime;
using Mefit_BE.Services.WorkoutServices;
using Mefit_BE.Models.DTOs.Workouts;
using Mefit_BE.Utils;
using Microsoft.AspNetCore.Authorization;
using Mefit_BE.Models.DTOs.Exercises;

namespace Mefit_BE.Controllers
{
    [Route("api/v1/workouts")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class WorkoutController : ControllerBase
    {
        private readonly IWorkoutService _service;
        private readonly IMapper _mapper;

        public WorkoutController(IWorkoutService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: api/v1/Workout
        /// <summary>
        /// Get all workouts from DB
        /// </summary>
        /// <returns>List of workouts</returns>
        [HttpGet]
        public async Task<ActionResult<ICollection<WorkoutDTO>>> GetWorkouts()
        {
            return Ok(_mapper.Map<ICollection<WorkoutDTO>>(await _service.GetAllAsync()));
        }

        // GET: api/v1/Workout/{id}
        /// <summary>
        /// Get Workout by its ID from DB
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Workout object</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<WorkoutDTO>> GetWorkout(int id)
        {
            try
            {
                return Ok(_mapper.Map<WorkoutDTO>(await _service.GetByIdAsync(id)));
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        // PUT: api/v1/Workout/{id}
        /// <summary>
        /// Update Workout by ID in DB
        /// </summary>
        /// <param name="id"></param>
        /// <param name="workoutDTO"></param>
        /// <returns>Action result</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWorkout(int id, WorkoutPutDTO workoutDTO)
        {
            if (id != workoutDTO.Workout_id)
            {
                return BadRequest();
            }
            if (!await _service.workoutExists(id))
            {
                return NotFound();
            }

            await _service.UpdateAsync(_mapper.Map<Workout>(workoutDTO));
            return NoContent();
        }

        // POST: api/v1/Workout
        /// <summary>
        /// Add new workout to DB
        /// </summary>
        /// <param name="workoutDTO"></param>
        /// <returns>Action result</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<WorkoutPostDTO>> PostWorkout(WorkoutPostDTO workoutDTO)
        {
            Workout workout = _mapper.Map<Workout>(workoutDTO);
            workout = await _service.AddAsync(workout);
            return CreatedAtAction("GetWorkout", new { id = workout.Workout_id }, workout);
        }

        // DELETE: api/v1/Workout/{id}
        /// <summary>
        /// Delete Workout by ID in DB
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Action result</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWorkout(int id)
        {
            if (!await _service.workoutExists(id))
            {
                return NotFound();
            }
            await _service.DeleteAsync(id);
            return NoContent();
        }

        // PUT: api/v1/{id}/sets
        /// <summary>
        /// Update Sets in a Workout by ID in DB
        /// </summary>
        /// <param name="SetId"></param>
        /// <param name="id">Workout Id</param>
        /// <returns>Action result</returns>
        [HttpPut("{id}/sets")]
        public async Task<IActionResult> UpdateSetsInWorkoutAsync(int[] SetId, int id)
        {
            await _service.UpdateSetsAsync(SetId, id);
            return NoContent();
        }


        /// <summary>
        /// Get all exercises in a workout by id
        /// </summary>
        /// <param name="id">Workout Id</param>
        /// <returns>Exercises List</returns>
        [HttpGet("{id}/exercises")]
        public async Task<ActionResult<ExerciseDTO>> GetExercisesInWorkoutAsync(int id)
        {
            return Ok(_mapper.Map<List<ExerciseDTO>>(await _service.GetExercisesAsync(id)));
        }
    }
}
