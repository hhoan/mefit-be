﻿using Microsoft.AspNetCore.Mvc;
using Mefit_BE.Models.Entities;
using Mefit_BE.Services.ProgramCategoryServices;
using AutoMapper;
using System.Net.Mime;
using Mefit_BE.Models.DTOs.ProgramCategories;
using Microsoft.AspNetCore.Authorization;
using Mefit_BE.Utils;

namespace Mefit_BE.Controllers
{
    [Route("api/v1/program-categories")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class ProgramCategoryController : ControllerBase
    {
        private readonly IProgramCategoryService _service;
        private readonly IMapper _mapper;

        public ProgramCategoryController(IProgramCategoryService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: api/v1/ProgramCategory
        /// <summary>
        /// Get all Program Categories from DB
        /// </summary>
        /// <returns>List of Program Categories</returns>
        [HttpGet]
        public async Task<ActionResult<ICollection<ProgramCategoryDTO>>> GetProgramCategories()
        {
            return Ok(_mapper.Map<ICollection<ProgramCategoryDTO>>(await _service.GetAllAsync()));
        }

        // GET: api/v1/ProgramCategory/{id}
        /// <summary>
        /// Get Program Category by ID from DB.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Program Category object</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ProgramCategoryDTO>> GetProgramCategory(int id)
        { 
            try
            {
                return Ok(_mapper.Map<ProgramCategoryDTO>(await _service.GetByIdAsync(id)));
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        // PUT: api/v1/ProgramCategory/{id}
        /// <summary>
        /// Update Program Category by ID in DB.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="programCategory"></param>
        /// <returns>Action Result</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProgramCategory(int id, ProgramCategoryPutDTO programCategory)
        {
            if (id != programCategory.Id)
            {
                return BadRequest();
            }
            if (!await _service.programCategoryExists(id))
            {
                return NotFound();
            }

            await _service.UpdateAsync(_mapper.Map<ProgramCategory>(programCategory));
            return NoContent();
        }

        // POST: api/v1/ProgramCategory
        /// <summary>
        /// Add new Program Category to DB.
        /// </summary>
        /// <param name="programCategoryDTO"></param>
        /// <returns>Action result</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ProgramCategoryPostDTO>> PostProgramCategory(ProgramCategoryPostDTO programCategoryDTO)
        {
            ProgramCategory programCategory = _mapper.Map<ProgramCategory>(programCategoryDTO);
            programCategory = await _service.AddAsync(programCategory);
            return CreatedAtAction("GetProgramCategory", new { id = programCategory.Id }, programCategory);
        }

        // DELETE: api/v1/ProgramCategory/{id}
        /// <summary>
        /// Detele Program Category by ID from DB.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Action Result</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProgramCategory(int id)
        {
            if (!await _service.programCategoryExists(id))
            {
                return NotFound();
            }
            await _service.DeleteAsync(id);
            return NoContent();
        }

    }
}
