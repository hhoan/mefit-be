﻿using Microsoft.AspNetCore.Mvc;
using Mefit_BE.Models.Entities;
using System.Net.Mime;
using Mefit_BE.Models.DTOs.UserProfileDTOs;
using AutoMapper;
using Mefit_BE.Services.UserProfileService;
using Microsoft.AspNetCore.Authorization;
using Mefit_BE.Models.DTOs.Exercises;
using Mefit_BE.Models.DTOs.Workouts;

namespace Mefit_BE.Controllers
{
    [Route("api/v1/user-profiles")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class UserProfileController : ControllerBase
    {
        private readonly IUserProfileService _UserProfileService;
        private readonly IMapper _mapper;
        public UserProfileController(IUserProfileService userProfileService, IMapper mapper)
        {
            _UserProfileService = userProfileService;
            _mapper = mapper;
        }

        // GET: api/v1/Profile
        /// <summary>
        /// Gets all userProfiles
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<ICollection<UserProfileDTO>>> GetUserProfiles()
        {
            return Ok(_mapper.Map<ICollection<UserProfileDTO>>(await _UserProfileService.GetAllAsync()));
        }

        // GET: api/v1/Profile/{id}
        /// <summary>
        /// Get UserProfile by ID 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<UserProfileDTO>> GetUserProfile(string id)
        {
            var UserProfile = await _UserProfileService.GetByIdAsync(id);

            if (UserProfile == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<UserProfileDTO>(UserProfile));
        }

        // PUT: api/v1/Profile/{id}
        /// <summary>
        /// Update UserProfile by ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProfile(string id, UserProfilePutDTO profile)
        {
            if (id != profile.Id)
            {
                return BadRequest();
            }
            if (!await _UserProfileService.UserProfileExists(id))
            {
                return NotFound();
            }

            await _UserProfileService.UpdateAsync(_mapper.Map<UserProfile>(profile));
            return NoContent();
        }

        // POST: api/v1/Profile
        /// <summary>
        /// Add new UserProfile to DB
        /// </summary>
        /// <param name="UserProfileDTO"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<UserProfilePostDTO>> PostUserProfile(UserProfilePostDTO UserProfileDTO)
        {
            UserProfile userProfile = _mapper.Map<UserProfile>(UserProfileDTO);
            await _UserProfileService.AddAsync(userProfile);
            return CreatedAtAction("GetUserProfile", new { id = userProfile.Id }, userProfile);
        }

        // DELETE: api/v1/Profile/{id}
        /// <summary>
        /// Delete UserProfile by ID from DB
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProfile(string id)
        {
            if (!await _UserProfileService.UserProfileExists(id))
            {
                return NotFound();
            }
            await _UserProfileService.DeleteAsync(id);
            return NoContent();
        }

        // PUT: api/v1/{id}/workouts
        /// <summary>
        /// Update Goals in UserProfile by ID in DB
        /// </summary>
        /// <param name="GoalId"></param>
        /// <param name="id">UserProfile Id</param>
        /// <returns>Action result</returns>
        [HttpPut("{id}/goals")]
        public async Task<IActionResult> UpdateWorkoutsInProgramAsync(int[] GoalId, string id)
        {
            await _UserProfileService.UpdateGoalsAsync(GoalId, id);
            return NoContent();
        }

        /// <summary>
        /// Get all exercises in a Goal by UserProfile ID
        /// </summary>
        /// <param name="id">UserProfile Id</param>
        /// <returns>Exercises List</returns>
        [HttpGet("{id}/exercises")]
        public async Task<ActionResult<ExerciseDTO>> GetExercisesInGoalAsync(string id)
        {
            return Ok(_mapper.Map<List<ExerciseDTO>>(await _UserProfileService.GetExercisesAsync(id)));
        } 

        /// <summary>
        /// Get all workouts in a Goal by UserProfile ID
        /// </summary>
        /// <param name="id">UserProfile Id</param>
        /// <returns>Workouts List</returns>
        [HttpGet("{id}/workouts")]
        public async Task<ActionResult<WorkoutSummaryDTO>> GetWorkoutsInGoalAsync(string id)
        {
            return Ok(_mapper.Map<List<WorkoutSummaryDTO>>(await _UserProfileService.GetWorkoutsAsync(id)));
        }
    }
}
