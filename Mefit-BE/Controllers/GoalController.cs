﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mefit_BE.Models;
using Mefit_BE.Models.Entities;
using System.Net.Mime;
using AutoMapper;
using Mefit_BE.Services.MuscleGroups;
using Mefit_BE.Services.GoalServices;
using Mefit_BE.Models.DTOs.MuscleGroups;
using Mefit_BE.Models.DTOs.GoalDTOs;
using Mefit_BE.Utils;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace Mefit_BE.Controllers
{
    [Route("api/v1/goals")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class GoalController : ControllerBase
    {
        private readonly IGoalService _service;
        private readonly IMapper _mapper;

        public GoalController(IGoalService goalService, IMapper mapper)
        {
            _service = goalService;
            _mapper = mapper;
        }

        // GET: api/v1/Goal
        /// <summary>
        /// Get all Goals from DB
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GoalDTO>>> GetGoals()
        {
            return Ok(_mapper.Map<List<GoalDTO>>(await _service.GetAllAsync()));
        }

        // GET: api/v1/Goal/{id}
        /// <summary>
        /// Get Goal by ID from DB
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<GoalDTO>> GetGoal(int id)
        {
            try
            {
                return Ok(_mapper.Map<GoalDTO>(await _service.GetByIdAsync(id)));
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        // GET: api/v1/goals/last
        /// <summary>
        /// Get last goal of user, from DB
        /// </summary>
        /// <returns></returns>
        [HttpGet("last")]
        public async Task<ActionResult<GoalDTO>> GetLastGoal()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId is null) return NotFound("User id not found");

            try
            {
                return Ok(_mapper.Map<GoalDTO>(
                    await _service.GetLastGoalOfCurrentUser(userId)
                    ));
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        // PUT: api/v1/Goal/{id}
        /// <summary>
        /// Update Goal by ID in DB
        /// </summary>
        /// <param name="id"></param>
        /// <param name="goal"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGoal(int id, GoalPutDTO goal)
        {
            if (id != goal.Goal_id)
            {
                return BadRequest();
            }
            if (!await _service.GoalExistsAsync(id))
            {
                return NotFound();
            }

            await _service.UpdateAsync(_mapper.Map<Goal>(goal));
            return NoContent();
        }

        // POST: api/v1/Goal
        /// <summary>
        /// Add new Goal to DB
        /// </summary>
        /// <param name="goalDTO"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Goal>> PostGoal(GoalPostDTO goalDTO)
        {
            Goal goal = _mapper.Map<Goal>(goalDTO);
            await _service.AddAsync(goal);
            return CreatedAtAction("GetGoal", new { id = goal.Goal_id }, goal);
        }

        // DELETE: api/v1/Goal/{id}
        /// <summary>
        /// Delete Goal by ID from DB
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Action result</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGoal(int id)
        {
            if (!await _service.GoalExistsAsync(id))
            {
                return NotFound();
            }
            await _service.DeleteAsync(id);
            return NoContent();
        }

        // PUT: api/v1/{id}/workouts
        /// <summary>
        /// Update Workout in a Goal by ID in DB
        /// </summary>
        /// <param name="WorkoutId"></param>
        /// <param name="id">Workout Id</param>
        /// <returns>Action result</returns>
        [HttpPut("{id}/workouts")]
        public async Task<IActionResult> UpdateWorkoutsInGoalsAsync(int[] WorkoutId, int id)
        {
            await _service.UpdateWorkoutsAsync(WorkoutId, id);
            return NoContent();
        }
    }
}
