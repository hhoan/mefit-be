﻿using Microsoft.AspNetCore.Mvc;
using Mefit_BE.Models.Entities;
using AutoMapper;
using Mefit_BE.Models.DTOs.Exercises;
using Mefit_BE.Services.Exercises;
using Mefit_BE.Utils;
using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;

namespace Mefit_BE.Controllers
{
    /// <summary>
    ///  Controller for Exercise domain
    /// </summary>

    [Route("api/v1/exercises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class ExerciseController : ControllerBase
    {
        // private readonly MeFitDbContext _context;
        // private readonly IMapper _mapper;
        private readonly IExerciseService _exerciseService;
        private readonly IMapper _mapper;

        public ExerciseController(IExerciseService exerciseService, IMapper mapper)
        {
            _exerciseService = exerciseService;
            _mapper = mapper;
        }

        // GET: api/v1/exercises
        /// <summary>
        /// Get all the exercises in the database
        /// </summary>
        /// <returns> List of exerciseDTO </returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ExerciseDTO>>> GetExercises()
        {
            return Ok(
                 _mapper.Map<List<ExerciseDTO>>(
                     await _exerciseService.GetAllAsync())
                 );
        }

        // GET: api/exercises/5
        /// <summary>
        ///  Get an exercise in the database based exercise ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns> A exerciseDto </returns>
        
        [HttpGet("{id}")]
        public async Task<ActionResult<ExerciseDTO>> GetExercise(int id)
        {
            try
            {
                return Ok(_mapper.Map<ExerciseDTO>(
                    await _exerciseService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        // POST: api/exercises/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Post a new exercise.
        /// </summary>
        /// <param name="exerciseDTO"></param>
        /// <returns> Createresponse for the newly created exercise </returns>
        [HttpPost]
        public async Task<ActionResult<Exercise>> PostExercise(ExercisePostDTO exerciseDTO)
        {
            Exercise exercise = _mapper.Map<Exercise>(exerciseDTO);
            await _exerciseService.AddAsync(exercise);

            return CreatedAtAction("GetExercise", new { id = exercise.Exercise_id }, exercise);
        }

        // PUT: api/exercise/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Update an exercise with the given exercise ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns> No content </returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutExercise(int id, ExercisePutDTO exercise)
        {
            if (id != exercise.Exercise_id)
            {
                return BadRequest();
            }

            try
            {
                await _exerciseService.UpdateAsync(_mapper.Map<Exercise>(exercise)
                    );
            }
            catch (EntityNotFoundException ex)
            {
                // throw;
            }

            return NoContent();
        }


        // DELETE: api/exercise/5
        /// <summary>
        /// Delete an exercise given by the exercise ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns> No content </returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteExercise(int id)
        {
            try
            {
                await _exerciseService.DeleteAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                // throw
                return NotFound();
            }
        }
    }
}
