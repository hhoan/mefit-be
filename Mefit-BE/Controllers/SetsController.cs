﻿using Microsoft.AspNetCore.Mvc;
using Mefit_BE.Models.Entities;
using AutoMapper;
using Mefit_BE.Models.DTOs.Sets;
using Mefit_BE.Services.Sets;
using Mefit_BE.Utils;
using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;

namespace Mefit_BE.Controllers
{
    /// <summary>
    ///  Controller for Set domain
    /// </summary>

    [Route("api/v1/sets")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class SetController : ControllerBase
    {
        // private readonly MeFitDbContext _context;
        // private readonly IMapper _mapper;
        private readonly ISetService _setService;
        private readonly IMapper _mapper;

        public SetController(ISetService setService, IMapper mapper)
        {
            _setService = setService;
            _mapper = mapper;
        }

        // GET: api/v1/sets
        /// <summary>
        /// Get all the sets in the database
        /// </summary>
        /// <returns> List of setDTO </returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SetDTO>>> GetSets()
        {
            return Ok(
                 _mapper.Map<List<SetDTO>>(
                     await _setService.GetAllAsync())
                 );
        }

        // GET: api/sets/5
        /// <summary>
        ///  Get an set in the database based set ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns> A setDto </returns>

        [HttpGet("{id}")]
        public async Task<ActionResult<SetDTO>> GetSet(int id)
        {
            try
            {
                return Ok(_mapper.Map<SetDTO>(
                    await _setService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        // POST: api/sets/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Post a new set.
        /// </summary>
        /// <param name="setDTO"></param>
        /// <returns> Createresponse for the newly created set </returns>
        [HttpPost]
        public async Task<ActionResult<Set>> PostSet(SetPostDTO setDTO)
        {
            Set set = _mapper.Map<Set>(setDTO);
            await _setService.AddAsync(set);

            return CreatedAtAction("GetSet", new { id = set.Set_id }, set);
        }

        // PUT: api/set/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Update an set with the given set ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns> No content </returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSet(int id, SetPutDTO set)
        {
            if (id != set.Set_id)
            {
                return BadRequest();
            }

            try
            {
                await _setService.UpdateAsync(_mapper.Map<Set>(set)
                    );
            }
            catch (EntityNotFoundException ex)
            {
                // throw;
            }

            return NoContent();
        }


        // DELETE: api/set/5
        /// <summary>
        /// Delete an set given by the set ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns> No content </returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSet(int id)
        {
            try
            {
                await _setService.DeleteAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                // throw
                return NotFound();
            }
        }
    }
}
