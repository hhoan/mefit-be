﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Mefit_BE.Models;
using Mefit_BE.Models.Entities;
using System.Net.Mime;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Mefit_BE.Services.MuscleGroups;
using Mefit_BE.Models.DTOs.MuscleGroups;
using AutoMapper;
using Mefit_BE.Utils;
using Mefit_BE.Models.DTOs.Exercises;
using Microsoft.AspNetCore.Authorization;

namespace Mefit_BE.Controllers
{
    /// <summary>
    ///  Controller for MuscleGroup domain
    /// </summary>

    [Route("api/v1/muscle-groups")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class MuscleGroupsController : ControllerBase
    {
        // private readonly MeFitDbContext _context;
        // private readonly IMapper _mapper;
        private readonly IMuscleGroupService _muscleGroupService;
        private readonly IMapper _mapper;

        public MuscleGroupsController(IMuscleGroupService muscleGroupService, IMapper mapper)
        {
            _muscleGroupService = muscleGroupService;
            _mapper = mapper;
        }

       // GET: api/v1/MuscleGroups
       /// <summary>
       /// Get all the musclegroup in the database
       /// </summary>
       /// <returns> List of MuscleGroupDTO </returns>
       [HttpGet]
        public async Task<ActionResult<IEnumerable<MuscleGroupDTO>>> GetMuscleGroups()
        {
            return Ok(
                 _mapper.Map<List<MuscleGroupDTO>>(
                     await _muscleGroupService.GetAllAsync())
                 );
        }

        // GET: api/Musclegroups/5
        /// <summary>
        ///  Get a muscle group in the database based on muscle group ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns> A MuscleGroup object </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MuscleGroupDTO>> GetMuscleGroup(int id)
        {
            try
            {
                return Ok(_mapper.Map<MuscleGroupDTO>(
                    await _muscleGroupService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }


        // GET: api/v1/musclegroups/2/exercises
        /// <summary>
        /// Get exercises for a muscle group from a given muscle group ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns> List of ExerciseSummaryDTO</returns>
        [HttpGet("{id}/exercises")]
        public async Task<ActionResult<IEnumerable<ExerciseSummaryDTO>>> GetExercisesForMuscleGroup(int id)
        {
            return Ok(
                _mapper.Map<List<ExerciseSummaryDTO>>(
                    await _muscleGroupService.GetExercisesAsync(id))
                );
        }



        // POST: api/Musclegroups/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Post a new muscle group.
        /// </summary>
        /// <param name="muscleGroupDTO"></param>
        /// <returns> Createresponse for the newly created muscle group </returns>
        [HttpPost]
        public async Task<ActionResult<MuscleGroup>> PostMuscleGroup(MuscleGroupPostDTO muscleGroupDTO)
        {
            MuscleGroup muscleGroup = _mapper.Map<MuscleGroup>(muscleGroupDTO);
            await _muscleGroupService.AddAsync(muscleGroup);

            return CreatedAtAction("GetMuscleGroup", new { id = muscleGroup.Id }, muscleGroup);
        }

        // PUT: api/musclegroup/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Update a muscle group with the given muscle group ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="muscleGroup"></param>
        /// <returns> No content </returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMuscleGroup(int id, MuscleGroupPutDTO muscleGroup)
        {
            if (id != muscleGroup.Id)
            {
                return BadRequest();
            }

            try
            {
                await _muscleGroupService.UpdateAsync(_mapper.Map<MuscleGroup>(muscleGroup)
                    );
            }
            catch (EntityNotFoundException ex)
            {
                // throw;
            }

            return NoContent();
        }


        // DELETE: api/musclegroup/5
        /// <summary>
        /// Delete a muscle group given by the muscle group ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns> No content </returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMuscleGroup(int id)
        {
            try
            {
                await _muscleGroupService.DeleteAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                // throw
                return NotFound();
            }
        }
    }
}
