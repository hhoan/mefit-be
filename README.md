﻿# Mefit Backend API
![build](https://img.shields.io/badge/build-passing-green)

This project is a part of a complete solutions. To see the frontend project click [here](https://gitlab.com/Vanessatpm/mefit-fe)

## Abstract
> An API was made for the Mefit application - an app to get fit and track your fitness goals and habits. 

> The API is built with Entity Framework (EF) core 7.0.3. The software is hosted on Gitlab with continuious integration / continuous deployment (CI/CD) to docker and azure. 

> The database is seeded with initial data to tailor the frontend user-interface.

> Part of the Noroff fullstack .NET accelerate module.

> The API is documented with Swagger.

## Description
MeFit is a part of the Noroff Accelerate course and is the final assignment that will mark our completion of the .NET fullstack program. The project creates a database in SQL Server with an Entity Framework Code First workflow and is constructed in ASP.NET Core Web API to allow users to manipulate the data. This features Controllers, Services, Mappers and DTOs to create a datastore and interface to store and manipulate MeFit DB tables. The database stores information about userprofiles, goals, programs, workouts, sets and exercises.

## Installation
You can download the repository with: 
```
git clone git@gitlab.com:hhoan/mefit-be.git 
```

## Usage

Create a secrets.json file in the Project folder to configure your connection string:
```json
{
  "ConnectionStrings": {
    "MyConnectionString": "Data Source = <Insert Connection string>; Initial Catalog = MeFitDb; Integrated Security = True; Trust Server Certificate = True;"
  }
}
```

In Package Manager Console run ```update-database``` to create the database in Microsoft SQL Server.

Run the project locally in Visual Studio.


## Database Schema

![MeFit DB schema](https://gitlab.com/hhoan/mefit-be/-/raw/develop/DBdesign.png)


## Dependencies
[![git](https://img.shields.io/badge/Git-F05032.svg?style=for-the-badge&logo=Git&logoColor=white)](https://git-scm.com/)
[![windows](https://img.shields.io/badge/Windows-0078D6.svg?style=for-the-badge&logo=Windows&logoColor=white)](https://www.microsoft.com/sv-se/windows)
[![.NET](https://img.shields.io/badge/.NET-512BD4.svg?style=for-the-badge&logo=dotnet&logoColor=white)](https://dotnet.microsoft.com/en-us/)
[![Microsoft SQL Server](https://img.shields.io/badge/Microsoft%20SQL%20Server-CC2927.svg?style=for-the-badge&logo=Microsoft-SQL-Server&logoColor=white)](https://learn.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16)
[![Visual Studio](https://img.shields.io/badge/Visual%20Studio-5C2D91.svg?style=for-the-badge&logo=Visual-Studio&logoColor=white)](https://visualstudio.microsoft.com/)

## Contributing

This project is unfortunately not open for contribution.

## Contributors
- [Abdullah Hussain](https://gitlab.com/abdullah-NO)
- [Ha Hoang](https://gitlab.com/hhoan) 
- [Vanessa Pastén-Millán](https://gitlab.com/Vanessatpm)
- [Lasse Steinnes](https://gitlab.com/lasse-steinnes)
- Ola Ellevold (UX/UI)
